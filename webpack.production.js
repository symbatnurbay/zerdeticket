const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')

const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: './client/index.js',
  output: {
    filename: './[name].js',
    path: path.join(__dirname, './client/build')
  },
  resolve: {
    extensions: ['.css', '.vue', '.js'],
    mainFiles: ['index'],
    alias: {
      vue: 'vue/dist/vue.js',
      Atoms: path.resolve(__dirname, './client/components/atoms/'),
      Organisms: path.resolve(__dirname, './client/components/organisms/'),
      Pages: path.resolve(__dirname, './client/components/pages/'),
      Molecules: path.resolve(__dirname, './client/components/molecules/'),
      EventBus: path.resolve(__dirname, './client/eventBus.js')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          cssModules: {
            localIdentName: '[path][name]---[local]---[hash:base64:5]',
            camelCase: true
          },
          extractCSS: true
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                camelCase: true,
                localIdentName: '[local]--[hash:base64:5]',
                importLoaders: 1
              }
            },
            'postcss-loader'
          ]
        })
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.EnvironmentPlugin([
      'NODE_ENV'
    ]),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new ExtractTextPlugin({
      filename: './[name].css',
      ignoreOrder: true
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './client/template.html'
    }),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'async'
    })
  ]
}
