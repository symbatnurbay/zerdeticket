import Vue from 'vue'
import Vuex from 'vuex'
import checkbox from './modules/checkbox'
import menu from './modules/menu'
import notification from './modules/notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    checkbox,
    menu,
    notification
  }
})
