const state = {
  keywordNotif: {},
  chairman: 0
}

const getters = {
  getKeywordNotif: state => state.keywordNotif,
  getChairman: state => state.chairman
}

const mutations = {
  changeKeywordNotif (state, payload) {
    state.keywordNotif = payload
  },
  deleteKeywordNotif (state, payload) {
    state.keywordNotif = {}
  },
  changeChairman (state, payload) {
    state.chairman = payload
  }
}

export default {
  state,
  getters,
  mutations
}
