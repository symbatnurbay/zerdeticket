const state = {
  checked: []
}

const getters = {
  getChecked: state => state.checked
}

const mutations = {
  emptyChecked (state) {
    state.checked = []
  },
  checkedAll (state, payload) {
    state.checked = payload.arr
  },
  checkedOneRemove (state, payload) {
    state.checked.forEach((item, index) => {
      if ((item === payload.id) || (item === 'all')) {
        state.checked.splice(index, 1)
      }
    })
  },
  checkedOneAdd (state, payload) {
    state.checked.push(payload.id)
  }
}

const actions = {
  changeChecked ({ commit, state, getters }, payload) {
    if (payload.type === 'all') {
      if (state.checked.indexOf('all') > -1) {
        commit('emptyChecked')
      } else {
        var temp = ['all']
        payload.value.forEach(item => {
          temp.push(item.id)
        })
        commit('checkedAll', { arr: temp })
      }
    } else {
      if (state.checked.indexOf(payload.value) > -1) {
        commit('checkedOneRemove', { id: payload.value })
      } else {
        commit('checkedOneAdd', { id: payload.value })
      }
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
