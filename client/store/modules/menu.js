const state = {
  currentPage: 'dashboard',
  currentNav: 'income',
  accessList: [],
  arrType: [
    {
      id: 'income',
      name: 'Входящие'
    },
    {
      id: 'ongoing',
      name: 'На исполнении'
    },
    {
      id: 'onagreement',
      name: 'На согласовании'
    },
    {
      id: 'done',
      name: 'Завершенные'
    }
  ]
}

const getters = {
  getCurrentPage: state => state.currentPage,
  getCurrentNav: state => state.currentNav,
  getAccessList: state => state.accessList,
  getArrType: state => state.arrType
}

const mutations = {
  changePage (state, payload) {
    state.currentPage = payload
  },
  changeNav (state, payload) {
    state.currentNav = payload
  },
  changeAccessList (state, payload) {
    state.accessList = payload
  }
}

export default {
  state,
  getters,
  mutations
}
