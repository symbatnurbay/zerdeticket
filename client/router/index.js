import VueRouter from 'vue-router'

import Registration from 'Pages/registration'
import Login from 'Pages/login'
import Main from 'Pages/dashboard'
import Forgot from 'Pages/forgot'
import Newpass from 'Pages/newpass'
import NewTicket from 'Pages/newTicket'
import TicketList from 'Pages/tickets'
import OneTicket from 'Pages/oneTicket'
import Settings from 'Pages/settings'
import Personal from 'Pages/settings/personal'
import One from 'Pages/settings/one'
import Clients from 'Pages/settings/clients'
import Staff from 'Pages/settings/staff'
import Roles from 'Pages/settings/roles'
import Departments from 'Pages/settings/departments'
import Auth from '../utils/auth'

const routes = [
  {
    path: '/',
    component: Main
  },
  {
    path: '/tickets',
    name: 'tickets',
    component: TicketList
  },
  {
    path: '/favorite',
    name: 'favorite',
    component: TicketList
  },
  {
    path: '/archived',
    name: 'archive',
    component: TicketList
  },
  {
    path: '/signup',
    component: Registration
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/forgot',
    component: Forgot
  },
  {
    path: '/newpass',
    component: Newpass
  },
  {
    path: '/addticket',
    component: NewTicket
  },
  {
    path: '/oneticket',
    component: OneTicket
  },
  {
    path: '/settings',
    component: Settings,
    children: [
      {
        path: 'personal',
        component: Personal,
        name: 'personal'
      },
      {
        path: 'clients',
        component: Clients,
        name: 'clients'
      },
      {
        path: 'roles',
        component: Roles,
        name: 'roles'
      },
      {
        path: 'departments',
        component: Departments,
        name: 'departments'
      },
      {
        path: ':type',
        component: Staff,
        name: 'type'
      },
      {
        path: ':type/:id',
        component: One,
        name: 'edit'
      }
    ]
  }
]

const router = new VueRouter({
  // mode: 'history',
  history: true,
  routes
})

router.beforeEach((to, from, next) => {
  if (Auth().isLoggedIn()) {
    if (to.path === '/login' || to.path === '/signup' || to.path === '/forgot' || to.path === '/newpass') {
      return next('/')
    } else if (to.path === '/' && Auth().currentUser().roleName === 'client') {
      return next('/tickets')
    } else next()
  } else {
    if (to.path !== '/signup' && to.path !== '/login' && to.path !== '/forgot' && to.path !== '/newpass') {
      next({
        path: '/login'
      })
    } else {
      next()
    }
  }
})

export default router
