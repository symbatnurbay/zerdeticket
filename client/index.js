import Vue from 'vue'
import VueRouter from 'vue-router'
import VueSocketio from 'vue-socket.io'

import App from './App.vue'

import Timeline from 'Atoms'
import router from './router'

import ApiUser from './utils/apiUser'
import ApiRole from './utils/apiRole'
import ApiHistory from './utils/apiHistory'
import ApiTicket from './utils/apiTicket'
import Api from './utils/api'
import ApiStats from './utils/stats'
import Auth from './utils/auth'
import MyFunc from './utils/functions'
import store from './store'
import VModal from 'vue-js-modal'
import Notifications from 'vue-notification'
import VueScrollTo from 'vue-scrollto'
import VueAutosize from 'vue-autosize'
const VueInputMask = require('vue-inputmask').default
// import Ckeditor from 'ckeditor'
// Vue.use(Ckeditor)
// require('ckeditor/ckeditor.js')
Vue.use(VueAutosize)
Vue.use(Notifications)
Vue.use(VModal)
Vue.use(VueScrollTo)

Vue.use(VueRouter)
Vue.use(Timeline)
Vue.use(VueInputMask)
// Vue.use(VueSocketio, 'http://localhost:8000/')
Vue.use(VueSocketio, 'http://178.62.231.241:8000/')

Vue.prototype.$apiUser = new ApiUser()
Vue.prototype.$apiRole = new ApiRole()
Vue.prototype.$apiHistory = new ApiHistory()
Vue.prototype.$apiTicket = new ApiTicket()
Vue.prototype.$api = new Api()
Vue.prototype.$apiStats = new ApiStats()
Vue.prototype.$auth = new Auth()
Vue.prototype.$util = new MyFunc()

const app = new Vue({
  router,
  store,
  ...App
})

app.$mount('#app')
