import axios from 'axios'

const getToken = () => {
  return window.localStorage['token']
}

const saveToken = token => {
  window.localStorage['token'] = token
}

const isLoggedIn = () => {
  const token = getToken()
  let payload
  if (token) {
    payload = token.split('.')[1]
    payload = window.atob(payload)
    payload = JSON.parse(payload)
    return payload.exp > Date.now() / 1000
  } else {
    return false
  }
}

const currentUser = () => {
  if (isLoggedIn()) {
    const token = getToken()
    let payload = token.split('.')[1]
    payload = window.atob(payload)
    payload = JSON.parse(payload)
    return {
      email: payload.email,
      username: payload.username,
      id: payload.id,
      role: payload.role,
      company: payload.company,
      roleName: payload.roleName
    }
  }
}

const checkCredentials = (user) => {
  let error = false
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const errorSchema = {}

  if (!user.password) {
    errorSchema.password = 'Введите пароль'
  }
  if (user.password !== user.rePassword) {
    errorSchema.rePassword = 'Пароли не совпадают'
  }
  if (!user.email) {
    errorSchema.email = 'Введите почту'
  }
  if (!re.test(user.email)) {
    errorSchema.email = 'Введите действительный адрес почты'
  }
  if (!user.username) {
    errorSchema.username = 'Введите имя пользователя'
  }
  if (!user.company) {
    errorSchema.company = 'Введите название компании'
  }

  Object.keys(errorSchema).forEach((item, index) => {
    if (item) {
      error = true
    } else {
      error = false
    }
  })
  if (error) {
    return {
      errorSchema,
      error
    }
  } else {
    return false
  }
}

const checkLoginCredentials = user => {
  let error = false
  const errorSchema = {}

  if (!user.email) {
    errorSchema.email = 'Введите ваш код'
  }
  if (!user.password) {
    errorSchema.password = 'Введите пароль'
  }

  Object.keys(errorSchema).forEach((item, index) => {
    if (item) {
      error = true
    } else {
      error = false
    }
  })
  if (error) {
    return {
      errorSchema,
      error
    }
  } else {
    return false
  }
}

const signup = (user) => {
  return new Promise((resolve, reject) => {
    if (checkCredentials(user).error) {
      reject(checkCredentials(user).errorSchema)
    } else {
      axios.post('/api/auth/signup', user)
        .then((res) => {
          saveToken(res.data.token)
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
    }
  })
}

const login = (user) => {
  return new Promise((resolve, reject) => {
    if (checkLoginCredentials(user).error) {
      reject(checkLoginCredentials(user).errorSchema)
    } else {
      axios.post('/api/auth/login', user)
        .then(res => {
          saveToken(res.data.token)
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
    }
  })
}

const forgot = (email) => {
  return new Promise((resolve, reject) => {
    axios.post('/api/auth/forgot', { email })
      .then(res => {
        resolve(res)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

const check = (email) => {
  return new Promise((resolve, reject) => {
    axios.post('/api/auth/check', { email })
      .then(res => {
        resolve(res)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

const change = (email, pass) => {
  return new Promise((resolve, reject) => {
    axios.post('/api/auth/change', { email, pass })
      .then(res => {
        resolve(res)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

const logout = () => {
  return new Promise((resolve, reject) => {
    window.localStorage.removeItem('token')
    resolve(true)
  })
}

export default () => {
  return {
    getToken,
    signup,
    isLoggedIn,
    logout,
    currentUser,
    login,
    forgot,
    check,
    change
  }
}
