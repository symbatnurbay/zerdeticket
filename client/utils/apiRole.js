import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getRoles: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/roles/')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addRole: (role) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/roles/', role)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editRole: (role) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/roles/edit', role)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteRole: (role, newRole) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/roles/delete', { role, newRole })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getPages: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/roles/pages')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    moveRoles: (arr) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/roles/move', arr)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
