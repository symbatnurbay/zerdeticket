import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getKeywordList: (value) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/keyword/getlist/${value}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getUsersList: (value) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/users/getlist/${value}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getUsersListTicket: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/users/list`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    assignUserChecked: (userId, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/tickets/assign`, { id, userId })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getTemplates: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/others/`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteNotification: (id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/notification/delete`, { id })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getDepList: (value) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/department/getdep/${value}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    checkNewTicket: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/notification/check`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addDepartment: (data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/department/add`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getDepartments: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/department/`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    editDepartment: (data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/department/edit`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteDepartment: (ids, newDep) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/department/delete`, { ids, newDep })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getNotifications: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/notification/`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeUnread: (arr) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/notification/`, arr)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getChairmanId: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/others/chairman`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    generateCode: () => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/others/generate`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getTelegramAccount: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/others/telegram`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    stopTelegram: () => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/others/stop`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteTelegram: () => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/others/delete`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
