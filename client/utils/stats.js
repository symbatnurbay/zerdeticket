import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getTodayStatistics: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/stats/today`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getTodayDone: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/stats/done`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getOngoing: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/stats/ongoing`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getDeadline: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/stats/deadline`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getAllTickets: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/stats/all`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getThisMonth: (month) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/stats/month/${month}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
