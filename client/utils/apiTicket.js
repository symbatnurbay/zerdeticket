import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    addTicket: (data) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/document/add', data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getTickets: (type) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/tickets/get/${type}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getFavorites: (type) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/tickets/fav/${type}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getArchive: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/tickets/archive`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeFav: (ids, value) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/fav', { ids, value })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getTicket: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/tickets/get/one/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeRead: (id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/tickets/read`, { id })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeDeadline: (id, date) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/tickets/deadline`, { id, date })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addAnswer: (data, id) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/document/answer/${id}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteTickets: (ids) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/tickets/delete`, ids)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    restore: (ids) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/tickets/restore`, ids)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteForever: (ids) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/tickets/forever`, ids)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeTypeChecked: (type, ids) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/tickets/move`, { type, ids })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getTicketsOfClient: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/tickets/client/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
