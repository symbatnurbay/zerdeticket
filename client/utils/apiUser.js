import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getCurrentUser: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/users/')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    updateMe: (data) => {
      return new Promise((resolve, reject) => {
        axios.put('/api/users/me', data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeAvatar: (id, file) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/users/${id}/avatar`, file)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteImage: (id) => {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/users/${id}/avatar`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addClient: (data) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/clients/add', data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getClients: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/clients/')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteClient: (data) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/clients/delete', data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getStaff: (type, action) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/staff/get/${type}/${action}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getBossList: (type, value) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/staff/getboss/${type}/${value}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getEmpList: (id, value) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/staff/getemp/${id}/${value}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addStaff: (type, action, data) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/staff/add/${type}/${action}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    checkStatus: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/staff/check/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteStaff: (id, newBoss, type) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/staff/delete/${type}/delete`, { id, newBoss })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getUser: (id, type) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/staff/one/${type}/edit/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    updateUser: (id, data, type) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/staff/edit/${type}/edit/${id}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
