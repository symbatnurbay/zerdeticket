export default function (option) {
  return {
    checkForEnglishLetters: (value) => {
      const letters = /^[A-Za-z]+$/

      if (value.match(letters)) return true
      else return false
    },
    checkForEnglishLettersAndNumbers: (value) => {
      const letterNumber = /^[0-9A-Za-z]+$/

      if (value.match(letterNumber)) return true
      else return false
    },
    profileHistory: (data, userId, userTo) => {
      var h = []
      var tempObj = {}
      var tempString = ''
      var tempString2 = ''
      Object.keys(data).map((key) => {
        if (key !== 'rePassword') {
          switch (key) {
            case 'name': tempString = 'изменили имя с'
              tempString2 = 'изменил/а имя с'
              break
            case 'companyName': tempString = 'изменили название компании с'
              tempString2 = 'изменил/а название компании с'
              break
            case 'companyEmail': tempString = 'изменили email компании с'
              tempString2 = 'изменил/а email компании с'
              break
            case 'companyPhone': tempString = 'изменили телефон компании с'
              tempString2 = 'изменил/а телефон компании с'
              break
            case 'companyAddress': tempString = 'изменили адрес компании с'
              tempString2 = 'изменил/а адрес компании с'
              break
            case 'companyCity': tempString = 'изменили город компании с'
              tempString2 = 'изменил/а город компании с'
              break
            case 'companyCountry': tempString = 'изменили страну компании с'
              tempString2 = 'изменил/а страну компании с'
              break
            case 'password': tempString = 'изменили свой пароль'
              tempString2 = 'изменил/а пароль'
              break
            case 'departmentName': tempString = 'изменили название департамента'
              tempString2 = 'изменил/а название департамента'
              break
            case 'role': tempString = data[key] && userTo[key] && data[key].id !== userTo[key].id ? 'изменили должность с' : ''
              tempString2 = data[key] && userTo[key] && data[key].id !== userTo[key].id ? 'изменил/а должность с' : ''
              break
            case 'boss': tempString = data[key] && userTo[key] && data[key].id !== userTo[key].id ? 'изменили руководителя с' : ''
              tempString2 = data[key] && userTo[key] && data[key].id !== userTo[key].id ? 'изменил/а руководителя с' : ''
              break
            case 'department': tempString = data[key] && userTo[key] && data[key].id !== userTo[key].id ? 'изменили департамент с' : ''
              tempString2 = data[key] && userTo[key] && data[key].id !== userTo[key].id ? 'изменил/а департамент с' : ''
              break
          }
          if (tempString && tempString !== '') {
            tempObj = {
              userFrom: userId,
              userTo: userTo.id,
              text: [tempString, 'на'],
              text2: [tempString2, 'на'],
              oldData: key === 'password' ? null : (key === 'role' || key === 'boss' || key === 'department') ? userTo[key].name : userTo[key],
              newData: key === 'password' ? null : (key === 'role' || key === 'boss' || key === 'department') ? data[key].name : data[key]
            }
            h.push(tempObj)
          }
        }
      })
      return h
    },
    avatarHistory: (userId, userTo) => {
      var tempObj = {
        userFrom: userId,
        userTo: userTo.id,
        text: ['изменили аватар'],
        text2: ['изменил/а аватар'],
        oldData: null,
        newData: null
      }
      return [tempObj]
    },
    staffHistory: (userFrom, userTo, type, ids, text) => {
      var textArr = []
      switch (type) {
        case 'assign': textArr = ['назначил/a протокол', 'для']
          break
        case 'archive': textArr = ['переместил/а протокол', 'в архив']
          break
        case 'answer': textArr = ['ответил/а на протокол']
          break
        case 'deadline': textArr = ['продлил/а дедлайн протокола']
          break
        case 'delete': textArr = ['удалил/а протокол', 'навсегда']
          break
        case 'favorite': textArr = [text]
          break
        case 'move': textArr = ['переместил/а протокол', text]
          break
        case 'new': textArr = ['добавил новый протокол']
          break
        case 'note': textArr = ['написал/а заметку в протоколе']
          break
        case 'restore': textArr = ['восстановил протокол']
          break
      }
      return {
        userFrom: userFrom,
        userTo: userTo,
        type: type,
        ticket: ids,
        text: textArr
      }
    },
    notification: (userFrom, userTo, type, ticketId, userTo2) => {
      var textArr = []
      switch (type) {
        case 'assign': textArr = ['назначил/a Вас ответственным за протокол']
          break
        case 'assignOther': textArr = ['поменял/a ответственного за протокол']
          break
        case 'archive': textArr = ['архивировал/а протокол']
          break
        case 'answer': textArr = ['ответил/а на протокол']
          break
        case 'deadline': textArr = ['изменил/а дедлайн протокола']
          break
        case 'delete': textArr = ['удалил/а протокол', 'навсегда']
          break
        case 'move': textArr = ['поменял/а тип протокола']
          break
        case 'note': textArr = ['упомянул/а Вас в комментарии к протоколу']
          break
        case 'restore': textArr = ['восстановил/а протокол']
          break
        case 'user': textArr = [`удалил/а пользователя ${userTo2}`]
          break
        case 'soon': textArr = ['До истечения дедлайна протокола', 'осталось 30 минут']
          break
        case 'late': textArr = ['Истек срок выполнения протокола']
          break
        case 'nobody': textArr = ['Ответственный не выбран для протокола']
          break
        case 'profile': textArr = ['изменил ваши личные данные']
          break
      }

      return {
        userFrom: userFrom,
        userTo: userTo,
        type: type,
        ticket: ticketId,
        text: textArr
      }
    }
  }
}
