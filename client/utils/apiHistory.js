import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    getHistoryProfile: (id) => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/history/profile/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getStaffHistory: () => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/history/staff`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
