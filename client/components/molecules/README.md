# MD TICKET MOLECULES

## Молекула историй

### Использование

```vue
<m-action-history history="YOUR_HISTORY"></m-action-history>
```
 или
```vue
<m-action-profile history="YOUR_HISTORY"></m-action-profile>
```

Молекула в качестве props принимает объект history. mActionHistory история действий по тикету, а mActionProfile история действий на странице профиля.
Все типы историй mActionHistory: favorite, delete, move, assign, answer, note, deadline, newticket. Список историй mActionHistory разные для клиента и сотрудника/админа. Клиент видит истории с типом delete, move, answer, newticket, в то время как сотрудник/админ видит все типы.

## Молекула клиента

### Использование

```vue
<m-client modificator="YOUR_MODIFICATOR" client="CLIENT_OBJECT"></m-client>
```

Молекула в качестве props принимает объект client и modificator с типом string. modificator может принимать значения staff или client.
Эта молекула используется в списке клиентов или сотрудников.

## Молекула datepicker

### Использование

```vue
<m-date-picker deadline="DATE" showtime="BOOLEAN_VALUE"></m-date-picker>
```

Молекула в качестве props принимает дату deadline и prop с типом boolean - showtime. Если showtime true, то в datepicker-е можно выбирать и время, и дату, а если false, то только дату.
Год можно выбрать начиная с нынешнего года до +5 лет.

## Молекула каждого тикета

### Использование

```vue
<m-each-ticket ticket="TICKET_OBJECT"></m-each-ticket>
```

Молекула в качестве props принимает объект ticket.
Эта молекула используется для отображения тикета на странице одного тикета.

## Молекула футера

### Использование

```vue
<m-footer></m-footer>
```

Молекула в качестве props может принимать переменную half типа Boolean. Если half === true, то размер футера будет меньше.

## Молекула InputGroup

### Использование

```vue
<m-input-group label="YOUR_LABEL" placeholder="YOUR_PLACEHOLDER" type="TYPE_OF_INPUT" value="VALUE_OF_INPUT" name="NAME_OF_INPUT" width="WIDTH_OF_INPUT"></m-input-group>
```

Этот виджет состоит из input field-а и label. Длина блока определяется через prop width, по дефолту оно 100%.

## Молекула Лого

### Использование

```vue
<m-logo titleColor="YOUR_COLOR" subtitle="YOUR_TEXT" subtitleColor="YOUR_COLOR" description="YOUR_DESCRIPTION" descriptionColor="YOUR_COLOR"></m-logo>
```

Эта молекула сделана так, что она подходит и для лого других проектов МД. titleColor определяет цвет слова MD, по дефолту оно #000 (черный). subtitle это большой текст рядом с главным названием. Этот текст означает название проекта, к примеру данный проект называется MD TICKET, получается subtitle этого проекта TICKET. Для проекта MD TIMELINE, subtitle будет слово TIMELINE. subtitleColor определяет цвет subtitle-а, по дефолту оно #fff (белый). description это краткое описание проекта, которое должно состоять из нескольких слов. descriptionColor определяет цвет описания, по дефолту #fff (белый).

## Молекула меню

### Использование

```vue
<m-menu type="TYPE_OF_MENU"></m-menu>
```

# Примечания

* Все молекулы должны иметь закрывающий тег
* Не забудьте сделать import перед использованием молекул и добавить в объект components. Не обязательно писать весь путь до файла, достаточно написать Molecules/название файла. Пример: import nameOfMolecule from 'Molecules/nameOfFile'. 

## Contributors

README crafted by :tiger: of Web development @symbatnurbay