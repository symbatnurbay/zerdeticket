# MD TICKET ATOMS

## Атом даты

### Использование

```vue
<a-date date="YOUR_DATE" format="YOUR_FORMAT" lang="YOU_LANGUAGE" modificator="YOUR_MODIFICATOR"></a-date>
```
Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
date        | Да  | Date, String | Принимает значения в формате [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) или [RFC 2822](https://tools.ietf.org/html/rfc2822#section-3.3) | Нет | Дата, которую нужно отобразить в указанном формате
format      | Нет | String       | D MMM [в] HH:mm, D MMM YYYY [в] HH:mm, L LT, D MMM, D MMMM YYYY, YYYY | D MMMM YYYY | Формат даты, как именно нужно отобразить дату
lang        | Нет | String       | Принимает один из значений указанных [здесь](https://github.com/moment/moment/tree/develop/locale) | ru (русский / Россия) | Язык на котором будет отображаться дата, а также будет использоваться местный формат.
modificator | Нет | String       | main, secondary, light, grey | main | Применяемый стиль для даты

### Описание поддерживаемых форматов

```javascript
YYYY                 // 2017
D MMMM YYYY          // 23 Сентября 2017
D MMM [в] HH:mm      // 23 Сент. в 03:32
D MMM YYYY [в] HH:mm // 23 Сент. 2017 в 03:32
D MMM                // 23 Сент.
L LT                 // 09.11.2017 09:57
```
### Описание стилей

* main - цвет текста черный
* secondary - цвет текста темно-серый
* light - цвет текста белый
* grey - цвет текста светло-серый

## Атом checkbox

### Использование

```vue
<a-checkbox modificator="YOUR_MODIFICATOR" label="YOUR_LABEL" checked="YOUR_VALUE" @change="YOUR_METHOD"> </a-checkbox>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
modificator | Нет | String | default, star | default | Принимаемый стиль для чекбокса
label | Нет | String | Любое | Нет | Имя для чекбокса, если нужно
checked | Нет | Boolean | true, false | false | Показывает нажат чекбокс или нет
@change | Нет | Function | Нет | Нет | Обработчик изменения состояния чекбокса, можете любой метод написать

# Атом аватарки

### Использование

```vue
<a-avatar src="YOUR_SRC" size="YOUR_SIZE"> </a-avatar>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
src | Нет | String | Любое | Фото анонима | Ссылка на фото пользователя
size | Нет | String | small, medium, big | small | Размер аватарки

### Описание размеров

* small - размер фото 30x30px
* medium - размер фото 40x40px
* big - размер фото 50x50px

# Атом кнопки

### Использование

```vue
<a-button type="YOUR_TYPE" modificator="YOUR_MODIFICATOR" text="YOUR_TEXT" name="NAME_OF_BUTTON" color="COLOR_OF_BUTTON" textType="TYPE_OF_TEXT" @click="YOUR_FUNCTION"> </a-button>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
type | Нет | String | submit, button | submit | Тип кнопки
modificator | Нет | String | ordinary, main | Нет | Стиль для кнопки
text | Нет | String | Любое | Нет | Текст кнопки
name | Нет | String | Смотрите список ниже | Нет | Название кнопки. Также используется для отображения иконки
color | Нет | String | pink, grey, none, noneGrey, greyLight | Нет | Определяет цвет кнопки и текста кнопки
textType | Нет | String | uppercase, none | uppercase | Определяет тип текст: верхний регистр или никакой
@click | Нет | Function | Нет | Нет | Обработчик клика на кнопку, можете любой метод написать

### Описание стилей

* ordinary - размер шрифта наследуется у parent-a; padding 5px; высота строки равна высоте шрифта указанного в root; используется, когда нужна кнопка с иконкой
* main - размер шрифта 14px; padding 10px; высота строки больше высоты шрифта указанного в root на 50%

### Все исконки для кнопки

![icons](static/images/icons.png)

# Атом инпута для загрузки файла

### Использование

```vue
<a-file-upload acceptType="YOUR_TYPES" multipleAllow="YOUR_VALUE"> </a-file-upload>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
acceptType | Нет | String | Список расширений можете посмотреть [здесь](https://en.wikipedia.org/wiki/List_of_file_formats) | .xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf | Типы файлов, которые можно загрузить
multipleAllow | Нет | Boolean | true, false | true | Определяет возможность загрузки нескольких файлов

# Атом grid

### Использование

```vue
<a-grid>
  <a-grid-item grid="YOUR_GRID_VALUE">
  </a-grid-item>
</a-grid>
```

Не принимает props-ов, только значения. Для подсчета ширину блока, разделяет страницу 12 ровные блоки.    
:exclamation: Не рекомендуется использовать в небольших компонентах.    
:exclamation: Не рекомендуется использовать, если не понимаете как работает сеточная верстка и как задаются значения в %-х.    
:exclamation: Не используйте как замена для div. Это может привести к неизвестным ошибкам во время верстки.

### Описание гридов

Значение | Описание 
-------- | ----------- 
grid-1-2, grid-6-12 | Использует 50% родительского блока
grid-7-12 | Использует 58.33% родительского блока
grid-1-12 | Использует 8.33% родительского блока
grid-1-6, grid-2-12 | Использует 16.66% родительского блока
grid-1-4, grid-3-12 | Использует 25% родительского блока
grid-1-3, grid-4-12 | Использует 33.33% родительского блока
grid-5-12 | Использует 41.66% родительского блока
grid-2-3, grid-8-12 | Использует 66.66% родительского блока
grid-3-4, grid-9-12 | Использует 75% родительского блока
grid-5-6, grid-10-12 | Использует 83.33% родительского блока
grid-11-12 | Использует 91.66% родительского блока

Made by @gohin_kz

# Атом инпута для текста

### Использование

```vue
<a-input value="YOUR_VALUE" placeholder="YOUR_PLACEHOLDER" name="NAME_OF_INPUT" type="TYPE_OF_INPUT"> </a-input>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
value | Да | String | Любое | Нет | Текст, который вы пишите в инпуте
placeholder | Нет | String | Любое | Нет | Если инпут пустой, то высвечивается этот текст
name | Нет | String | Любое | Нет | Название элемента
type | Нет | String | hidden, password, text, email | text | Определяет тип вводимого текста

# Атом ссылки

### Использование

```vue
<a-link href="YOUR_URL" text="YOUR_TEXT" type="TYPE_OF_LINK" color="YOUR_COLOR" target="YOUR_TARGET"> </a-link>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
href | Да | String | Любое | Нет | Ссылка на какую-то страницу
text | Нет | String | Любое | Нет | Текст ссылки, которая отображается
color | Нет | String | grey, pink | grey | Цвет текста ссылки
type | Нет | String | main | Нет | Если выбран main, то text-decoration: none
target | Нет | String | _blank | Нет | Если выбран blank, то ссылка открывается на новой странице 

# Атом кнопка меню

### Использование

```vue
<a-menu-button name="MENU_BUTTON_NAME" active="YOUR_VALUE"> </a-menu-button>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
name | Да | String | open, pending, closed, add, settings, exit, notification | Нет | Название элемента. В зависимости от названия определяется вид кнопки
active | Нет | Boolean | true, false | false | Определяет статус кнопки

# Атом параграф

### Использование

```vue
<a-paragraph textHtml="YOUR_TEXT"> </a-paragraph>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
textHtml | Нет | String | Любое | Нет | Текст, который нужно отобразить в виде html

# Атом select

### Использование

```vue
<a-select name="NAME_OF_SELECT" color="YOU_COLOR" arrow="YOUR_ARROW" text="YOUR_TEXT" align="YOUR_ALIGN" type="YOUR_TYPE" showbg="YOUR_VALUE"> </a-select>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
name | Да | String | Любое | Нет | Название селекта
color | Нет | String | pink, grey, greyLight | Нет | Определяет цвет элемента и текста
arrow | Нет | String | up, down | down | Определяет направление стрелки селекта, а также где расположение списка элементов селекта (сверху или снизу)
text | Нет | String | Любое | Нет | Текст отображаемый на кнопке селекта
align | Нет | String | left, center, right | left | Определяет расположение списка элементов селекта
type | Да | String | user, send, assign, snooze | Нет | Тип селекта. Определяет какой список должен выводиться

# Атом Textarea

### Использование

```vue
<a-textarea placeholder="YOUR_PLACEHOLDER" value="YOUR_VALUE" name="NAME_OF_TEXTAREA" refUsers="LIST_OF_USERS" focus="YOUR_VALUE"> </a-textarea>
```

Props | Обязательно | Тип | Принимаемые значения | Значение по умолчанию | Описание
----- | ----------- | --- | -------------------- | --------------------- | --------
placeholder | Нет | String | Любое | Нет | Если textarea пустой, то высвечивается этот текст
value | Да | String | Любое | Нет | Текст, который вы пишите в textarea
name | Нет | String | Любое | Нет | Название textarea
refUsers | Нет | Array | Любое | Нет | Список пользователей, которых вы хотите отметить
focus | Нет | Boolean | true, false | false | Определяет фокус на textarea

:exclamation: Если вы хотите использовать **refUsers**, то **name** обязательно должен быть равно **note**

# Примечания

* Все атомы должны иметь закрывающий тег
* Если modificator кнопки ordinary, а также указан название кнопки из списка, то будет использоваться кнопка с иконкой. Если не хотите использовать иконку, но при этом хотите, чтобы modificator был ordinary, то стоит указать другое название для кнопки (то есть название кнопки не должен быть в списке).

## Contributors

README crafted by @misterybray :baby_chick: and :tiger: of Web development @symbatnurbay