import vueButton from './button/index.vue'
import vueDate from './date/index.vue'
import vueLink from './link/index.vue'
import vueParagraph from './paragraph/index.vue'
import vueInput from './input/index.vue'
import vueGrid from './grid/index.vue'
import vueGridItem from './grid/gridItem.vue'
import vueFileUpload from './fileUpload/index.vue'
import vueSelect from './select/index.vue'
import vueAvatar from './avatar/index.vue'
import vueTextarea from './textarea/index.vue'
import vueCheckbox from './checkbox/index.vue'
import vueMenuButton from './menuButton/index.vue'

const atomButton = {
  install: (Vue) => {
    Vue.component('a-button', vueButton)
  }
}

const atomGrid = {
  install: (Vue) => {
    Vue.component('a-grid', vueGrid)
  }
}

const atomGridItem = {
  install: (Vue) => {
    Vue.component('a-grid-item', vueGridItem)
  }
}

const atomDate = {
  install: (Vue) => {
    Vue.component('a-date', vueDate)
  }
}

const atomLink = {
  install: (Vue) => {
    Vue.component('a-link', vueLink)
  }
}

const atomParagraph = {
  install: (Vue) => {
    Vue.component('a-paragraph', vueParagraph)
  }
}

const atomInput = {
  install: (Vue) => {
    Vue.component('a-input', vueInput)
  }
}

const atomSelect = {
  install: (Vue) => {
    Vue.component('a-select', vueSelect)
  }
}

const atomFileUpload = {
  install: (Vue) => {
    Vue.component('a-file-upload', vueFileUpload)
  }
}

const atomAvatar = {
  install: (Vue) => {
    Vue.component('a-avatar', vueAvatar)
  }
}

const atomTextarea = {
  install: (Vue) => {
    Vue.component('a-textarea', vueTextarea)
  }
}

const atomCheckbox = {
  install: (Vue) => {
    Vue.component('a-checkbox', vueCheckbox)
  }
}

const atomMenuButton = {
  install: (Vue) => {
    Vue.component('a-menu-button', vueMenuButton)
  }
}

export {
  atomButton,
  atomDate,
  atomLink,
  atomParagraph,
  atomInput,
  atomGrid,
  atomGridItem,
  atomFileUpload,
  atomSelect,
  atomAvatar,
  atomTextarea,
  atomCheckbox,
  atomMenuButton
}
