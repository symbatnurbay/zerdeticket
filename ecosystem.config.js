module.exports = {
  apps: [
    {
      name: 'zerdeticket',
      script: './index.js',
      env: {
        'NODE_ENV': 'production'
      }
    }
  ]
}
