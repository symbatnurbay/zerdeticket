import express from 'express'
const router = new express.Router()

import ctrlNotification from '../controllers/notification'
import { checkStaff } from '../middlewares/index'

router.post('/delete', ctrlNotification.deleteNotification)
router.get('/check', checkStaff, ctrlNotification.checkNewTicket)
router.get('/', ctrlNotification.getNotifications)
router.post('/', ctrlNotification.changeUnread)

module.exports = router
