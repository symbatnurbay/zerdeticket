import express from 'express'
const router = new express.Router()

import ctrlStaff from '../controllers/staff'
import { checkAccess } from '../middlewares/index'

router.get('/get/:type/:action', checkAccess, ctrlStaff.getStaff)
router.get('/getboss/:type/:value', ctrlStaff.getBosslist)
router.get('/getemp/:department/:value', ctrlStaff.getEmplist)
router.post('/add/:type/:action', checkAccess, ctrlStaff.addStaff)
router.post('/delete/:type/:action', checkAccess, ctrlStaff.deleteStaff)
router.get('/check/:id', ctrlStaff.checkStatus)
router.get('/one/:type/:action/:id', checkAccess, ctrlStaff.getOne)
router.post('/edit/:type/:action/:id', checkAccess, ctrlStaff.updateOne)

module.exports = router
