import express from 'express'
const router = new express.Router()

import ctrlOthers from '../controllers/others'
import ctrlTelegram from '../controllers/telegram'
import { checkStaff } from '../middlewares/index'

router.get('/', ctrlOthers.getTemplates)
router.get('/chairman', checkStaff, ctrlOthers.getChairmanId)
router.post('/generate', ctrlTelegram.generateCode)
router.get('/telegram', ctrlTelegram.getTelegramAccount)
router.post('/stop', ctrlTelegram.stopTelegram)
router.post('/delete', ctrlTelegram.deleteTelegram)

module.exports = router
