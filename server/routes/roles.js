import express from 'express'
const router = new express.Router()

import ctrlRoles from '../controllers/roles'
import { checkAdmin } from '../middlewares/index'

router.get('/', checkAdmin, ctrlRoles.getRoles)
router.post('/', checkAdmin, ctrlRoles.addRole)
router.post('/edit', checkAdmin, ctrlRoles.editRole)
router.post('/delete', checkAdmin, ctrlRoles.deleteRole)
router.post('/move', checkAdmin, ctrlRoles.moveRole)
router.get('/pages', ctrlRoles.getPages)

module.exports = router
