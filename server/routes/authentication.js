import express from 'express'
const router = new express.Router()

import ctrlAuth from '../controllers/authentication'

router.post('/signup', ctrlAuth.signup)
router.post('/login', ctrlAuth.login)
router.post('/forgot', ctrlAuth.forgot)
router.post('/check', ctrlAuth.check)
router.post('/change', ctrlAuth.change)

module.exports = router
