import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'

const storage = multer.diskStorage({
  destination: 'static/images/avatars',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage })

import ctrlUser from '../controllers/user'
import { checkStaff } from '../middlewares/index'

router.get('/', ctrlUser.getCurrentUser)
router.put('/me', ctrlUser.updateMe)
router.post('/:id/avatar', upload.single('avatar'), ctrlUser.changeAvatar)
router.delete('/:id/avatar', ctrlUser.deleteImage)
router.get('/getlist/:value', ctrlUser.getUsersList)
router.get('/list', checkStaff, ctrlUser.getUsersListTicket)

module.exports = router
