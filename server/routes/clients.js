import express from 'express'
const router = new express.Router()

import ctrlClients from '../controllers/clients'
import { checkAdmin } from '../middlewares/index'

router.post('/add', checkAdmin, ctrlClients.addClient)
router.get('/', checkAdmin, ctrlClients.getClients)
router.post('/delete', checkAdmin, ctrlClients.deleteClients)

module.exports = router
