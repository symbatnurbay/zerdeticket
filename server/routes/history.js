import express from 'express'
const router = new express.Router()

import ctrlHistory from '../controllers/history'

router.get('/profile/:id', ctrlHistory.getProfileHistory)
router.get('/staff', ctrlHistory.getStaffHistory)

module.exports = router
