import express from 'express'
const router = new express.Router()

import ctrlStats from '../controllers/statistics'

router.get('/today', ctrlStats.getTodayStatistics)
router.get('/done', ctrlStats.getTodayDone)
router.get('/ongoing', ctrlStats.getOngoing)
router.get('/deadline', ctrlStats.getDeadline)
router.get('/all', ctrlStats.getAllTickets)
router.get('/month/:month', ctrlStats.getThisMonth)

module.exports = router
