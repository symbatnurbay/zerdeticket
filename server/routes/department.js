import express from 'express'
const router = new express.Router()

import ctrlDepartment from '../controllers/department'
import { checkAdmin, checkStaff } from '../middlewares/index'

router.get('/', checkAdmin, ctrlDepartment.getDepartments)
router.post('/add', checkAdmin, ctrlDepartment.addDepartment)
router.post('/edit', checkAdmin, ctrlDepartment.editDepartment)
router.post('/delete', checkAdmin, ctrlDepartment.deleteDepartment)
router.get('/getdep/:value', checkStaff, ctrlDepartment.geDepList)

module.exports = router
