import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'

const storage = multer.diskStorage({
  destination: 'static/files',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage })

import ctrlDocument from '../controllers/document'
import { checkTicket } from '../middlewares/index'

router.post('/add', upload.array('fileToUpload[]'), ctrlDocument.addTicket)
router.post('/answer/:id', checkTicket, upload.array('fileToUpload[]'), ctrlDocument.addAnswer)

module.exports = router
