import express from 'express'
const router = new express.Router()

import ctrlTicket from '../controllers/ticket'
import { checkTicket, checkStaffTicket, checkTicketList } from '../middlewares/index'

router.get('/get/:type', ctrlTicket.getTickets)
// router.get('/get/:type/:page', ctrlTicket.getTicketsByType)
router.get('/get/one/:id', checkTicket, ctrlTicket.getOneTicket)
router.get('/fav/:type', ctrlTicket.getFavorites)
router.get('/archive', ctrlTicket.getArchive)
router.post('/fav', ctrlTicket.changeFav)
router.post('/read', checkStaffTicket, ctrlTicket.changeRead)
router.post('/deadline', checkStaffTicket, ctrlTicket.changeDeadline)
router.post('/delete', ctrlTicket.deleteTickets)
router.post('/restore', ctrlTicket.restoreTickets)
router.post('/forever', checkTicketList, ctrlTicket.deleteForever)
router.post('/move', checkTicketList, ctrlTicket.changeTypeChecked)
router.post('/assign', checkStaffTicket, ctrlTicket.assignUserChecked)
router.get('/client/:id', ctrlTicket.getTicketsOfClient)

module.exports = router
