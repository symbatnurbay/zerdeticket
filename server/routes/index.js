import express from 'express'
const router = express.Router()
// import { getNewMessages } from '../controllers/email'

import { checkToken } from '../middlewares/index'

router.use('/auth', require('./authentication'))
router.use('/users', checkToken, require('./user'))
router.use('/tickets', checkToken, require('./ticket'))
router.use('/clients', checkToken, require('./clients'))
router.use('/roles', checkToken, require('./roles'))
router.use('/staff', checkToken, require('./staff'))
router.use('/history', checkToken, require('./history'))
router.use('/document', checkToken, require('./document'))
router.use('/others', checkToken, require('./others'))
router.use('/notification', checkToken, require('./notification'))
router.use('/department', checkToken, require('./department'))
router.use('/stats', checkToken, require('./statistics'))
// router.use('/email', auth, require('./email'))

router.get('*', (req, res) => {
  res.redirect('/#' + req.originalUrl)
})

// отслеживает письма пользователей
// getNewMessages()

// если возникла какая-то ошибка, то если в обработке запроса мы пишем next(error), все попадает сюда
// router.use((err, req, res, next) => {
//   console.error(err.stack)

//   if (err.name === 'UnauthorizedError') {
//     return res.status(401).send('invalid token...')
//   }
//   res.status(500).send({ message: err.message })
// })

module.exports = router
