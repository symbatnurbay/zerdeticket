import { Client, Query } from 'pg'
import jwt from 'jsonwebtoken'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const checkToken = (req, res, next) => {
  const token = req.headers['authorization']
  if (!token) res.status(400).send({ message: 'Invalid token' }).end()
  else {
    const q = token.split(' ')[1]
    jwt.verify(q, Configs.auth.JWT_SECRET, (err, result) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Invalid token' }).end()
      } else {
        client.query(new Query(`SELECT u.id FROM users u INNER JOIN user_company uc ON uc.user_id = u.id WHERE u.id = $1 AND u.email = $2 AND u.username = $3
          AND u.role_id = $4 AND uc.company_id = $5`, [result.id, result.email, result.username, result.role, result.company]), (err, user) => {
          if (err) {
            console.log(err)
            res.status(400).send({ message: 'Invalid token' }).end()
          } else if (user.rowCount === 0) {
            res.status(400).send({ message: 'Invalid token' }).end()
          } else {
            req.payload = result
            next()
          }
        })
      }
    })
  }
}

const checkAdmin = (req, res, next) => {
  if (req.payload) {
    if (req.payload.roleName === 'chairman') next()
    else res.status(401).send({ message: 'Отказано в доступе' }).end()
  } else res.status(401).send({ message: 'Отказано в доступе' }).end()
}

const checkAccess = (req, res, next) => {
  if (req.payload) {
    if (req.payload.roleName === 'chairman') next()
    else {
      client.query(new Query(`SELECT id FROM roles WHERE code = $1 AND company_id = $2`, [req.params.type, req.payload.company]), (err, roleId) => {
        if (err) {
          console.log(err)
          res.status(401).send({ message: 'Отказано в доступе' }).end()
        } else if (roleId.rowCount === 0) res.status(404).send({ message: 'Страница не найдена' }).end()
        else {
          client.query(new Query(`SELECT id FROM role_access WHERE role_id = $1 AND access_role = $2 AND action = $3`,
            [req.payload.role, roleId.rows[0].id, req.params.action]), (err, accessId) => {
            if (err) {
              console.log(err)
              res.status(401).send({ message: 'Отказано в доступе' }).end()
            } else if (accessId.rowCount === 0) res.status(401).send({ message: 'Отказано в доступе' }).end()
            else next()
          })
        }
      })
    }
  } else res.status(401).send({ message: 'Отказано в доступе' }).end()
}

const checkTicket = (req, res, next) => {
  if (req.payload) {
    client.query(new Query(`SELECT t.client_id FROM tickets t INNER JOIN user_company uc ON t.client_id = uc.user_id
      WHERE t.id = $1 AND uc.company_id = $2`, [req.params.id, req.payload.company]), (err, result) => {
      if (err) {
        console.log(err)
        res.status(401).send({ message: 'Отказано в доступе' }).end()
      } else if (result.rowCount > 0 && req.payload.roleName !== 'client') next()
      else if (result.rowCount > 0 && req.payload.roleName === 'client' && req.payload.id === result.rows[0].client_id) next()
      else res.status(401).send({ message: 'Отказано в доступе' }).end()
    })
  } else res.status(401).send({ message: 'Отказано в доступе' }).end()
}

const checkStaffTicket = (req, res, next) => {
  if (req.payload && req.payload.roleName !== 'client') {
    client.query(new Query(`SELECT t.id FROM tickets t INNER JOIN user_company uc ON t.client_id = uc.user_id
      WHERE t.id = $1 AND uc.company_id = $2`, [req.body.id, req.payload.company]), (err, result) => {
      if (err) {
        console.log(err)
        res.status(401).send({ message: 'Отказано в доступе' }).end()
      } else if (result.rowCount > 0) next()
      else res.status(401).send({ message: 'Отказано в доступе' }).end()
    })
  } else res.status(401).send({ message: 'Отказано в доступе' }).end()
}

const checkTicketList = (req, res, next) => {
  if (req.payload) {
    var ticketIds = req.body.ids ? req.body.ids : req.body
    if (ticketIds[0] === 'all') {
      ticketIds.splice(0, 1)
    }
    client.query(new Query(`SELECT t.client_id FROM tickets t INNER JOIN user_company uc ON t.client_id = uc.user_id
      WHERE t.id = ANY ($1) AND uc.company_id = $2`, [ticketIds, req.payload.company]), (err, result) => {
      if (err) {
        console.log(err)
        res.status(401).send({ message: 'Отказано в доступе' }).end()
      } else if (result.rowCount > 0 && req.payload.roleName !== 'client') next()
      else if (result.rowCount > 0 && req.payload.roleName === 'client' && req.payload.id === result.rows[0].client_id) next()
      else res.status(401).send({ message: 'Отказано в доступе' }).end()
    })
  } else res.status(401).send({ message: 'Отказано в доступе' }).end()
}

const checkStaff = (req, res, next) => {
  if (req.payload && req.payload.roleName !== 'client') next()
  else res.status(401).send({ message: 'Отказано в доступе' }).end()
}

module.exports = {
  checkToken,
  checkAdmin,
  checkAccess,
  checkTicket,
  checkStaffTicket,
  checkTicketList,
  checkStaff
}
