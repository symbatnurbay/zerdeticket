import passport from 'passport'
import { Client, Query } from 'pg'
import { validPassword } from '../utils/personal'
import { Strategy as LocalStrategy } from 'passport-local'

const client = new Client('postgres://symbat:symbat@localhost:5432/zerdeticket')
client.connect()

passport.use(new LocalStrategy({
  usernameField: 'email',
  passReqToCallback: true
}, (req, username, password, done) => {
  client.query(new Query(`SELECT u.*, uc.company_id, r.code AS "roleName" FROM users u INNER JOIN user_company uc ON uc.user_id = u.id
    INNER JOIN roles r ON u.role_id = r.id WHERE (u.email = $1 OR u.username = $1) AND status = 'active'`, [username]), (err, result) => {
    if (err) return done(err)
    else {
      if (result.rowCount === 0) {
        return done(null, false, { message: 'Пользователь не найден', origin: 'email' })
      } else {
        if (result.rows[0].salt && result.rows[0].hash && validPassword(password, result.rows[0].salt, result.rows[0].hash)) {
          return done(null, result.rows[0])
        } else {
          return done(null, false, { message: 'Неправильное имя пользователя или пароль', origin: 'password' })
        }
      }
    }
  })
}))
