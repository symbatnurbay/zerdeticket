import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS keywords(id SERIAL PRIMARY KEY, word VARCHAR(255), department_id INT REFERENCES departments(id))`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
