import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS history_profile(id SERIAL PRIMARY KEY, user_from INT REFERENCES users(id),
  user_to INT REFERENCES users(id), created_at timestamptz, old_value VARCHAR(255), new_value VARCHAR(255),
  text_one VARCHAR(255) ARRAY, text_two VARCHAR(255) ARRAY)`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
