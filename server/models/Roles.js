import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS roles(id SERIAL PRIMARY KEY, name VARCHAR(255), code VARCHAR(255), delete BOOLEAN,
  company_id INT REFERENCES companies(id), level INT)`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
