import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, name VARCHAR(255),
  email VARCHAR(255), username VARCHAR(30), status VARCHAR(10), avatar VARCHAR(255),
  hash VARCHAR(255), salt VARCHAR(255), role_id INT REFERENCES roles(id), created_at timestamptz)`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
