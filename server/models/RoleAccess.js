import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS role_access(id SERIAL PRIMARY KEY, role_id INT REFERENCES roles(id), access_role INT REFERENCES roles(id), action VARCHAR(100))`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
