import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS history_staff(id SERIAL PRIMARY KEY, user_from INT REFERENCES users(id),
  user_to INT REFERENCES users(id), created_at timestamptz, type VARCHAR(25), ticket_id INT REFERENCES tickets(id),
  text VARCHAR(255) ARRAY)`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
