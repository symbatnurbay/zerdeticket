import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS tickets(id SERIAL PRIMARY KEY, title VARCHAR(255),
  ticket_number INT, type VARCHAR(20), favorite BOOLEAN, status VARCHAR(20), deadline timestamptz,
  created_at timestamptz, read BOOLEAN, client_id INT REFERENCES users(id),
  user_id INT REFERENCES users(id), income_date timestamptz, closed_date timestamptz)`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
