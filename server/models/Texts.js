import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS texts(id SERIAL PRIMARY KEY, text TEXT, plain_text TEXT, doc json,
  type VARCHAR(20), created_at timestamptz, files json, user_id INT REFERENCES users(id),
  ref_users INT ARRAY, ticket_id INT REFERENCES tickets(id))`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
