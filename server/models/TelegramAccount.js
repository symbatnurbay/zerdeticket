import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS telegram_accounts(id SERIAL PRIMARY KEY, telegram_id VARCHAR(255), first_name VARCHAR(255),
  last_name VARCHAR(40), username VARCHAR(100), user_id INT REFERENCES users (id), active BOOLEAN, code VARCHAR(255))`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
