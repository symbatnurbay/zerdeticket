import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS user_department(id SERIAL PRIMARY KEY, user_id INT REFERENCES users(id),
  department_id INT REFERENCES departments(id), director BOOLEAN)`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
