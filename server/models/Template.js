import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS templates(id SERIAL PRIMARY KEY, name VARCHAR(255), text TEXT, company_id INT REFERENCES companies(id))`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
