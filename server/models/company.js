import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS companies(id SERIAL PRIMARY KEY, email VARCHAR(255),
  name VARCHAR(255), phone VARCHAR(25), address VARCHAR(255), city VARCHAR(255), country VARCHAR(255))`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
