import { Client } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

client.query(`CREATE TABLE IF NOT EXISTS boss_emp(id SERIAL PRIMARY KEY, boss_id INT REFERENCES users(id), emp_id INT REFERENCES users(id))`)
  .then(() =>
    client.end()
  ).catch(err =>
    console.log(err)
  )
