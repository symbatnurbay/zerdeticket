import { Client, Query } from 'pg'
import { checkTelegramAcc } from '../controllers/telegram'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

module.exports = (io) => {
  io.sockets.on('connection', (socket) => {
    socket.on('createRoom', (roomName) => {
      socket.join(roomName)
    })

    socket.on('leaveRoom', (roomName) => {
      socket.leave(roomName)
    })

    socket.on('addProfileHistory', (data) => {
      data.newHistory.forEach((item) => {
        client.query(new Query(`INSERT INTO history_profile (user_from, user_to, created_at, old_value, new_value, text_one, text_two)
          VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`, [item.userFrom, item.userTo, new Date(), item.oldData, item.newData,
            item.text, item.text2]), (err, historyId) => {
          if (err) console.log(err)

          client.query(new Query(`SELECT hp.created_at, hp.old_value, hp.new_value, hp.text_one, hp.text_two,
            (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name FROM users u1 WHERE u1.id = hp.user_from) uf) AS "userFrom",
            (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = hp.user_to) ut) AS "userTo"
            FROM history_profile hp WHERE hp.id = $1`, [historyId.rows[0].id]), (err, history) => {
            if (err) console.log(err)

            io.sockets.in(data.roomName).emit('profileHistoryAdded', history.rows[0])
          })
        })
      })
    })

    socket.on('addStaffHistory', (data) => {
      data.newHistory.ticket.forEach(item => {
        client.query(new Query(`INSERT INTO history_staff (user_from, user_to, created_at, type, ticket_id, text) VALUES ($1, $2, $3, $4, $5, $6)
          RETURNING id`, [data.newHistory.userFrom, data.newHistory.userTo, new Date(), data.newHistory.type, item, data.newHistory.text]), (err, historyId) => {
          if (err) console.log(err)

          client.query(new Query(`SELECT hs.created_at, hs.text, hs.type,
            (SELECT row_to_json(td) FROM (SELECT t.id, t.ticket_number FROM tickets t WHERE t.id = hs.ticket_id) td) AS ticket,
            (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name, u1.avatar FROM users u1 WHERE u1.id = hs.user_from) uf) AS "userFrom",
            (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = hs.user_to) ut) AS "userTo"
            FROM history_staff hs WHERE hs.id = $1`, [historyId.rows[0].id]), (err, history) => {
            if (err) console.log(err)

            io.sockets.in(data.roomName).emit('historyStaffAdded', history.rows[0])
          })
        })
      })
    })

    socket.on('addNotification', (data) => {
      client.query(new Query(`INSERT INTO notifications (user_from, user_to, created_at, type, ticket_id, text, read) VALUES ($1, $2, $3, $4, $5, $6, $7)
        RETURNING id`, [data.userFrom, data.userTo, new Date(), data.type, data.ticket, data.text, false]), (err, notifId) => {
        if (err) console.log(err)

        client.query(new Query(`SELECT nf.id, nf.created_at, nf.text, nf.type, nf.read,
          (SELECT row_to_json(td) FROM (SELECT t.id, t.ticket_number FROM tickets t WHERE t.id = nf.ticket_id) td) AS ticket,
          (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name, u1.avatar FROM users u1 WHERE u1.id = nf.user_from) uf) AS "userFrom",
          (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = nf.user_to) ut) AS "userTo"
          FROM notifications nf WHERE nf.id = $1`, [notifId.rows[0].id]), (err, notif) => {
          if (err) console.log(err)

          checkTelegramAcc(data.userTo, notif.rows[0])
          io.sockets.in(`user-${data.userTo}`).emit('notificationAdded', notif.rows[0])
        })
      })
    })
  })
}
