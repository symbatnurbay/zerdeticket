import kue from 'kue'
import async from 'async'

const jobs = require('../queue/').jobs

const createJob = (ticket, company, type, time) => {
  var timeTodelete = new Date()
  // timeTodelete.setTime(new Date().getTime() + 30000)
  if (type === 'nobody') timeTodelete.setTime(new Date().getTime() + time)
  else timeTodelete.setTime(new Date(ticket.deadline).getTime() - time)

  jobs.delayed((err, ids) => {
    if (err) console.error(err)
    else {
      async.forEachOf(ids, (value, key, callback) => {
        kue.Job.get(value, (err, job) => {
          if (err) callback(err)
          else if ((job.type === type) && (job.data.ticket === ticket.id)) {
            console.log(`job removed: type - ${type}, data - ${ticket.id}`)
            kue.Job.remove(value)
            callback()
          } else callback()
        })
      }, (err) => {
        if (err) console.error(err)
        else {
          var job = jobs.create(type, { ticket: ticket.id, company })
            .delay(timeTodelete)
            .priority('high')
            .removeOnComplete(true)
            .save()
          jobs.process('overlord')
        }
      })
    }
  })
}

const createTelegramJob = (data) => {
  var timeTodelete = new Date()
  timeTodelete.setTime(new Date().getTime() + 86400000)

  var job = jobs.create('telegram', data)
    .delay(timeTodelete)
    .priority('high')
    .removeOnComplete(true)
    .save()
  jobs.process('overlord')
}

module.exports = {
  createJob,
  createTelegramJob
}
