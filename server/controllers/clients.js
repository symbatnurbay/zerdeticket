import { Client, Query } from 'pg'
import mailsender from '../utils/mail'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const addClient = (req, res) => {
  if (!req.body.username || !req.body.email || !req.body.name) {
    res.status(400).send({ message: 'Все поля должны быть заполнены' }).end()
  } else {
    client.query(new Query(`SELECT id FROM users WHERE username = $1`, [req.body.username]), (err, clientUsername) => {
      if (clientUsername.rowCount > 0) {
        res.status(403).send({ message: 'Пользователь с таким логином уже существует', origin: 'username' }).end()
      } else {
        client.query(new Query(`SELECT id FROM users WHERE email = $1`, [req.body.email]), (err, clientEmail) => {
          if (clientEmail.rowCount > 0) {
            res.status(403).send({ message: 'Пользователь с таким email-м уже существует', origin: 'email' }).end()
          } else {
            client.query(new Query(`SELECT id FROM roles WHERE name = $1`, ['client']), (err, roleName) => {
              if (roleName.rowCount === 0) {
                client.query(new Query(`INSERT INTO roles (name, code, delete) VALUES ('client', 'client', $1) RETURNING id`, [false]), (err, newRole) => {
                  add(req.body, req.payload.company, newRole.rows[0].id, (err, newClient) => {
                    if (err) {
                      console.log(err)
                      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                    } else {
                      res.status(200).send({ message: 'Клиент успешно создан', user: newClient })
                    }
                  })
                })
              } else {
                add(req.body, req.payload.company, roleName.rows[0].id, (err, newClient) => {
                  if (err) {
                    console.log(err)
                    res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                  } else {
                    res.status(200).send({ message: 'Клиент успешно создан', user: newClient })
                  }
                })
              }
            })
          }
        })
      }
    })
  }
}

const add = (data, company, role, callback) => {
  client.query(new Query(`INSERT INTO users (name, email, username, status, avatar, role_id, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *`,
    [data.name, data.email, data.username, 'active', '/images/photo-placeholder.png', role, new Date()]), (err, newClient) => {
    if (err) callback(err)
    else {
      client.query(new Query(`INSERT INTO user_company (user_id, company_id) VALUES ($1, $2)`, [newClient.rows[0].id, company]))
      const link = `http://${Configs.serverData.addressProd}/#/newpass?user=${newClient.rows[0].email}`
      const emailText = `Здравствуйте, ${newClient.rows[0].name} <br><br> Вы были зарегистрированы в системе ZERDE Ticket (zerde-ticket.tk). <br><br> Для того, чтобы закончить регистрацию, пожалуйста, перейдите по ссылке:
${link} <br><br> С уважением, команда проекта ZERDE Ticket`
      mailsender.sendMail(newClient.rows[0].email, 'Регистрация ZERDE Ticket', emailText, emailText, (err) => {
        if (err) {
          console.log(err)
        }
      })
      callback(null, newClient.rows[0])
    }
  })
}

const getClients = (req, res) => {
  client.query(new Query(`SELECT u.id, u.name, u.email, u.username, u.avatar FROM users u INNER JOIN user_company uc ON u.id = uc.user_id
    WHERE u.role_id = (SELECT id FROM roles WHERE name = $1) AND u.status = $2 AND uc.company_id = $3 ORDER BY u.created_at DESC`,
    ['client', 'active', req.payload.company]), (err, clients) => {
    if (err) {
      console.log(err)
      res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(clients.rows)
  })
}

const deleteClients = (req, res) => {
  if (req.body[0] === 'all') {
    req.body.splice(0, 1)
  }
  client.query(new Query(`UPDATE users SET status = $1 WHERE id = ANY ($2)`, ['archived', req.body]))
  client.query(new Query(`UPDATE tickets SET status = $1 WHERE client_id = ANY ($2)`, ['archived', req.body]))
  res.status(200).end()
}

module.exports = {
  addClient,
  getClients,
  deleteClients,
  add
}
