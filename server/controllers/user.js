import { Client, Query } from 'pg'
import fs from 'fs'
import path from 'path'
import { setPassword } from '../utils/personal'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const getCurrentUser = (req, res) => {
  getUserWithCompany(req.payload.id, (err, user) => {
    if (err) {
      console.log(err)
      return res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else res.status(200).send(user)
  })
}

const updateMe = (req, res) => {
  if (req.body.password && req.body.rePassword) {
    if (req.body.password !== req.body.rePassword) {
      return res.status(400).send({ message: 'Пароли не совпадают', origin: 'rePassword' })
    } else {
      const { hash, salt } = setPassword(req.body.password)
      client.query(new Query(`UPDATE users SET hash = $1, salt = $2 WHERE id = $3`, [hash, salt, req.payload.id]))
    }
  }
  if (req.body.departmentName) {
    client.query(new Query(`UPDATE departments SET name = $1 WHERE id = $2`, [req.body.departmentName, req.body.departmentId]))
  }
  client.query(new Query(`SELECT * FROM users WHERE id = $1`, [req.payload.id]), (err, result) => {
    if (err) return res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })

    const user = {
      name: req.body.name || result.rows[0].name
    }
    client.query(new Query(`UPDATE users SET name = $1 WHERE id = $2`, [user.name, req.payload.id]))

    client.query(new Query(`SELECT code FROM roles WHERE id = $1`, [req.payload.role]), (err, roleName) => {
      if (err) return res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })

      if (roleName.rows[0].code === 'chairman') {
        client.query(new Query(`SELECT * FROM companies WHERE id = $1`, [req.payload.company]), (err, companyData) => {
          if (err) return res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })

          const company = {
            name: req.body.companyName || companyData.rows[0].name,
            email: req.body.companyEmail || companyData.rows[0].email,
            phone: req.body.companyPhone || companyData.rows[0].phone,
            address: req.body.companyAddress || companyData.rows[0].address,
            city: req.body.companyCity || companyData.rows[0].city,
            country: req.body.companyCountry || companyData.rows[0].country
          }
          client.query(new Query(`UPDATE companies SET name = $1, email = $2, phone = $3, address = $4, city = $5, country = $6 WHERE id = $7`,
            [company.name, company.email, company.phone, company.address, company.city, company.country, req.payload.company]))
        })
      }
    })
    getUserWithCompany(req.payload.id, (err, userCompany) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
      } else res.status(200).send({ message: 'Данные успешно обновлены', user: userCompany })
    })
  })
}

const changeAvatar = (req, res) => {
  const avatar = `/images/avatars/${req.file.filename}`
  const query = client.query(new Query(`SELECT * FROM users WHERE id = $1`, [req.params.id]))
  query.on('end', (result) => {
    if (result.rows[0].avatar && result.rows[0].avatar !== '/images/photo-placeholder.png') {
      fs.unlink(path.join('./static', result.rows[0].avatar), err => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
        } else {
          client.query(new Query(`UPDATE users SET avatar = $1 WHERE id = $2 RETURNING avatar`, [avatar, req.params.id]), (err, newAvatar) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
            } else res.status(200).send({ message: 'Аватар успешно изменен', avatar: newAvatar.rows[0].avatar })
          })
        }
      })
    } else {
      client.query(new Query(`UPDATE users SET avatar = $1 WHERE id = $2 RETURNING avatar`, [avatar, req.params.id]), (err, newAvatar) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
        } else res.status(200).send({ message: 'Аватар успешно изменен', avatar: newAvatar.rows[0].avatar })
      })
    }
  })
}

const deleteImage = (req, res) => {
  const query = client.query(new Query(`SELECT * FROM users WHERE id = $1`, [req.params.id]))
  query.on('end', (result) => {
    fs.unlink(path.join('./static', result.rows[0].avatar), err => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
      } else {
        client.query(new Query(`UPDATE users SET avatar = $1 WHERE id = $2 RETURNING avatar`, ['/images/photo-placeholder.png', req.params.id]), (err, newAvatar) => {
          if (err) {
            console.log(err)
            res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
          } else res.status(200).send({ message: 'Аватар успешно удален', avatar: newAvatar.rows[0].avatar })
        })
      }
    })
  })
}

const getUserWithCompany = (id, callback) => {
  client.query(new Query(`SELECT u.id, u.email, u.username, u.status, u.name, u.avatar, c.name AS "companyName", c.email AS "companyEmail",
    c.phone AS "companyPhone", c.address AS "companyAddress", c.city AS "companyCity", c.country AS "companyCountry", row_to_json(r) as role,
    d.name AS "departmentName", d.id AS "departmentId" FROM user_company uc INNER JOIN users u ON uc.user_id = u.id
    INNER JOIN companies c ON uc.company_id = c.id INNER JOIN roles r ON r.id = u.role_id LEFT JOIN user_department ud ON ud.user_id = u.id 
    LEFT JOIN departments d ON d.id = ud.department_id WHERE uc.user_id = $1`, [id]), (err, user) => {
    if (err) callback(err)

    callback(null, user.rows[0])
  })
}

const getUsersList = (req, res) => {
  var tempArr = req.params.value.split(' ')
  var tempStr = tempArr[tempArr.length - 1].replace(/[^\w\s]/gi, '')
  client.query(new Query(`SELECT u.id, u.username, u.name FROM users u INNER JOIN user_company uc ON u.id = uc.user_id
    INNER JOIN roles r ON u.role_id = r.id WHERE uc.company_id = $1 AND r.code <> 'client' AND
    (UPPER(u.name) LIKE UPPER($2) OR UPPER(u.username) LIKE UPPER($2))`, [req.payload.company, `%${tempStr}%`]), (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else res.status(200).send(list.rows)
  })
}

const getUsersListTicket = (req, res) => {
  if (req.payload.roleName === 'counselor') {
    client.query(new Query(`SELECT u.name, u.username, u.id, r.name AS "roleName" FROM users u INNER JOIN user_company uc ON u.id = uc.user_id
      INNER JOIN roles r ON u.role_id = r.id WHERE uc.company_id = $1 AND r.code = 'chairman'`, [req.payload.company]), (err, user) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
      } else res.status(200).send(user.rows)
    })
  } else {
    client.query(new Query(`SELECT level FROM roles WHERE code = 'chairman' AND company_id = $1`, [req.payload.company]), (err, chairmanLvl) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
      } else {
        client.query(new Query(`SELECT level FROM roles WHERE code = 'director' AND company_id = $1`, [req.payload.company]), (err, directorLvl) => {
          if (err) {
            console.log(err)
            res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
          } else {
            if (req.payload.roleName === 'director') {
              client.query(new Query(`SELECT u.name, u.username, u.id, r.name AS "roleName" FROM users u INNER JOIN user_company uc ON u.id = uc.user_id
                INNER JOIN roles r ON u.role_id = r.id LEFT JOIN user_department ud ON u.id = ud.user_id WHERE
                (u.id = ANY (SELECT user_id FROM user_department WHERE department_id = (SELECT department_id FROM user_department WHERE user_id = $1) AND director = false)
                OR u.id = (SELECT boss_id FROM boss_emp WHERE emp_id = $1)) AND uc.company_id = $2 ORDER BY r.level`,
                [req.payload.id, req.payload.company]), (err, user) => {
                if (err) {
                  console.log(err)
                  res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
                } else res.status(200).send(user.rows)
              })
            } else {
              client.query(new Query(`SELECT level FROM roles WHERE id = $1`, [req.payload.role]), (err, myLvl) => {
                if (err) {
                  console.log(err)
                  res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
                } else {
                  if (myLvl.rows[0].level > directorLvl.rows[0].level) {
                    client.query(new Query(`SELECT u.name, u.username, u.id, r.name AS "roleName" FROM users u 
                      INNER JOIN roles r ON u.role_id = r.id INNER JOIN user_department ud ON u.id = ud.user_id
                      WHERE ud.department_id = (SELECT department_id FROM user_department WHERE user_id = $1) AND
                      ud.director = true ORDER BY r.level`, [req.payload.id]), (err, user) => {
                      if (err) {
                        console.log(err)
                        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
                      } else res.status(200).send(user.rows)
                    })
                  } else {
                    client.query(new Query(`SELECT u.name, u.username, u.id, r.name AS "roleName" FROM users u 
                      INNER JOIN roles r ON u.role_id = r.id WHERE u.id = (SELECT boss_id FROM boss_emp WHERE emp_id = $1) OR
                      u.id = ANY (SELECT emp_id FROM boss_emp WHERE boss_id = $1) ORDER BY r.level`, [req.payload.id]), (err, user) => {
                      if (err) {
                        console.log(err)
                        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
                      } else res.status(200).send(user.rows)
                    })
                  }
                }
              })
            }
          }
        })
      }
    })
  }
}

module.exports = {
  getCurrentUser,
  updateMe,
  changeAvatar,
  deleteImage,
  getUsersList,
  getUsersListTicket
}
