import { Client, Query } from 'pg'
import { addKeyword } from './keyword'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const getDepartments = (req, res) => {
  client.query(new Query(`SELECT d.id, d.name, (SELECT row_to_json(d1) FROM (SELECT u.id, u.name FROM users u
    INNER JOIN user_department ud ON u.id = ud.user_id WHERE ud.department_id = d.id AND ud.director = true) d1) AS director,
    (SELECT COUNT(*) FROM user_department ud2 INNER JOIN users u2 ON u2.id = ud2.user_id WHERE ud2.department_id = d.id AND u2.status = 'active') AS empNumber,
    array_to_string(array(SELECT k.word FROM keywords k WHERE k.department_id = d.id), ', ') AS keywords
    FROM departments d WHERE d.company_id = $1`, [req.payload.company]), (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else res.status(200).send(list.rows)
  })
}

const addDepartment = (req, res) => {
  client.query(new Query(`INSERT INTO departments (name, company_id) VALUES ($1, $2) RETURNING *`,
    [req.body.name, req.payload.company]), (err, depId) => {
    if (err) {
      console.log(err)
      res.status(200).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else {
      const keywords = req.body.keywords.split(',')
      addKeyword(keywords, depId.rows[0].id)
      var newDep = depId.rows[0]
      newDep.keywords = req.body.keywords
      res.status(200).send({ newDep, message: 'Департамент успешно добавлен' }).end()
    }
  })
}

const editDepartment = (req, res) => {
  if (req.body.keywords) {
    const keywords = req.body.keywords.split(',')
    addKeyword(keywords, req.body.id)
  }
  client.query(new Query(`SELECT d.name, (SELECT ud.user_id FROM user_department ud WHERE ud.department_id = d.id AND ud.director = true) AS director
    FROM departments d WHERE d.id = $1`, [req.body.id]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(200).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else {
      if (result.rows[0].name !== req.body.name) {
        client.query(new Query(`UPDATE departments SET name = $1 WHERE id = $2`, [req.body.name, req.body.id]))
      }
      if (req.body.director && result.rows[0].director !== req.body.director.id) {
        client.query(new Query(`UPDATE users SET role_id = (SELECT id FROM roles WHERE code = 'specialist') WHERE id = $1`, [result.rows[0].director]))
        client.query(new Query(`UPDATE user_department SET director = false WHERE user_id = $1`, [result.rows[0].director]))
        client.query(new Query(`UPDATE users SET role_id = (SELECT id FROM roles WHERE code = 'director') WHERE id = $1`, [req.body.director.id]))
        client.query(new Query(`UPDATE user_department SET director = true WHERE user_id = $1`, [req.body.director.id]))
        client.query(new Query(`DELETE FROM boss_emp WHERE emp_id = $1 RETURNING boss_id`, [result.rows[0].director]), (err, boss) => {
          if (err) {
            console.log(err)
            res.status(200).send({ message: 'Неизвестная ошибка, попробуйте позже' })
          } else {
            client.query(new Query(`INSERT INTO boss_emp (boss_id, emp_id) VALUES ($1, $2)`, [boss.rows[0].boss_id, req.body.director.id]))
          }
        })
      }
      res.status(200).send({ message: 'Данные департамента успешно изменены' })
    }
  })
}

const deleteDepartment = (req, res) => {
  var tempDep = req.body.ids[0] === 'all' ? null : req.body.newDep
  if (req.body.ids[0] === 'all') {
    req.body.ids.splice(0, 1)
  }
  client.query(new Query(`UPDATE user_department SET director = false WHERE department_id = ANY ($1) AND director = true RETURNING user_id`,
    [req.body.ids]), (err, userIds) => {
    if (err) {
      console.log(err)
      res.status(200).send({ message: 'Неизвестная ошибка, попробуйте позже' })
    } else {
      var ids = []
      userIds.rows.forEach(item => {
        ids.push(item.user_id)
      })
      client.query(new Query(`UPDATE users SET role_id = (SELECT id FROM roles WHERE code = 'specialist') WHERE id = ANY ($1)`, [ids]))
      client.query(new Query(`DELETE FROM boss_emp WHERE emp_id = ANY ($1)`, [ids]))
      if (tempDep) {
        client.query(new Query(`UPDATE user_department SET department_id = $1 WHERE department_id = ANY ($2)`, [tempDep, req.body.ids]))
      } else {
        client.query(new Query(`DELETE FROM user_department WHERE department_id = ANY ($1)`, [req.body.ids]))
      }
      client.query(new Query(`DELETE FROM keywords WHERE department_id = ANY ($1)`, [req.body.ids]))
      client.query(new Query(`DELETE FROM departments WHERE id = ANY ($1)`, [req.body.ids]))
      res.status(200).send({ message: 'Выбранные департаменты успешно удалены' })
    }
  })
}

const geDepList = (req, res) => {
  client.query(new Query(`SELECT id, name FROM departments WHERE UPPER(name) LIKE UPPER($1) AND company_id = $2`,
    [`%${req.params.value}%`, req.payload.company]), (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(list.rows).end()
  })
}

module.exports = {
  addDepartment,
  geDepList,
  getDepartments,
  editDepartment,
  deleteDepartment
}
