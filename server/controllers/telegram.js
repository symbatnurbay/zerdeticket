import randomstring from 'randomstring'
import { createTelegramJob } from './jobProcesses'
import { Client, Query } from 'pg'
import { sendMsg } from './telegramProcesses'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const generateCode = (req, res) => {
  const code = randomstring.generate()
  client.query(new Query(`INSERT INTO telegram_accounts (user_id, active, code) VALUES ($1, $2, $3) RETURNING *`,
    [req.payload.id, false, code]), (err, resultId) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      createTelegramJob(resultId.rows[0])
      res.status(200).send(resultId.rows[0]).end()
    }
  })
}

const getTelegramAccount = (req, res) => {
  client.query(new Query(`SELECT * FROM telegram_accounts WHERE user_id = $1`, [req.payload.id]), (err, account) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(account.rows[0]).end()
  })
}

const stopTelegram = (req, res) => {
  client.query(new Query(`UPDATE telegram_accounts SET active = NOT active WHERE user_id = $1 RETURNING active, telegram_id`,
    [req.payload.id]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      const message = result.rows[0].active ? 'Уведомления в телеграм бот успешно подключены' : 'Уведомления в телеграм бот отключены'
      sendMsg(result.rows[0].telegram_id, message)
      res.status(200).send({ message })
    }
  })
}

const deleteTelegram = (req, res) => {
  client.query(new Query(`DELETE FROM telegram_accounts WHERE user_id = $1 RETURNING telegram_id`, [req.payload.id]), (err, telegramId) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      const message = 'Ваш телеграм аккаунт успешно отвязан от системы ZERDE Ticket'
      sendMsg(telegramId.rows[0].telegram_id, message + '. Если хотите заново подключиться нажмите кнопку Подключиться в настройках системы и напишите в боте /start')
      res.status(200).send({ message })
    }
  })
}

const checkTelegramAcc = (userId, notification) => {
  client.query(new Query(`SELECT telegram_id FROM telegram_accounts WHERE user_id = $1 AND active = true`, [userId]), (err, telegramAcc) => {
    if (err) console.log(err)
    else if (telegramAcc.rowCount > 0) {
      var text = notification.type === 'keyword' ? `Был добавлен новый протокол, который подходит вам по ключевым словам. Не забудьте проверить.`
        : `${notification.userFrom ? notification.userFrom.username + ' ' : ''}${notification.text[0]}${notification.ticket ? ' #' + notification.ticket.ticket_number : ''} ${notification.text[1] ? notification.text[1] : ''}`
      sendMsg(telegramAcc.rows[0].telegram_id, text)
    }
  })
}

module.exports = {
  generateCode,
  getTelegramAccount,
  stopTelegram,
  deleteTelegram,
  checkTelegramAcc
}
