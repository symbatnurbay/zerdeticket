import { Client, Query } from 'pg'
import { deleteKeywordNotif } from './notification'
import { createJob } from './jobProcesses'
import moment from 'moment'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const getTickets = (req, res) => {
  if (req.payload.roleName === 'client') {
    if (req.params.type === 'done') {
      client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.created_at, t.type, t.status, t.user_id,
        (SELECT txt.plain_text AS text FROM texts txt WHERE txt.ticket_id = t.id AND txt.created_at = (SELECT MAX(tt.created_at)
        FROM texts tt WHERE tt.ticket_id = t.id)) AS text FROM tickets t WHERE t.client_id = $1 AND t.status = 'active'
        AND t.type = $2 ORDER BY t.created_at DESC`, [req.payload.id, req.params.type]), (err, ticketList) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(ticketList.rows)
      })
    } else {
      client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.created_at, t.type, t.status, t.user_id,
        (SELECT txt.plain_text AS text FROM texts txt WHERE txt.ticket_id = t.id AND txt.created_at = (SELECT MAX(tt.created_at)
        FROM texts tt WHERE tt.ticket_id = t.id)) AS text FROM tickets t WHERE t.client_id = $1 AND t.status = 'active'
        AND t.type <> $2 ORDER BY t.created_at DESC`, [req.payload.id, 'done']), (err, ticketList) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(ticketList.rows)
      })
    }
  } else {
    if (req.params.type === 'all') {
      client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.type, t.favorite, t.created_at, t.read, t.user_id, t.deadline, t.status,
        (SELECT row_to_json(c) FROM (SELECT cli.id, cli.username, cli.name, cli.avatar FROM users cli WHERE cli.id = t.client_id) c) AS client,
        (SELECT row_to_json(ttt) FROM (SELECT txt.plain_text AS text, txt.created_at FROM texts txt WHERE txt.ticket_id = t.id AND
        txt.created_at = (SELECT MAX(tt.created_at) FROM texts tt WHERE tt.ticket_id = t.id)) ttt) AS text FROM tickets t 
        INNER JOIN user_company uc ON t.client_id = uc.user_id WHERE t.status = 'active' AND uc.company_id = $1 ORDER BY read, deadline`,
        [req.payload.company]), (err, ticketList) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(ticketList.rows)
      })
    } else {
      client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.type, t.favorite, t.created_at, t.read, t.user_id, t.deadline, t.status,
        (SELECT row_to_json(c) FROM (SELECT cli.id, cli.username, cli.name, cli.avatar FROM users cli WHERE cli.id = t.client_id) c) AS client,
        (SELECT row_to_json(ttt) FROM (SELECT txt.plain_text AS text, txt.created_at FROM texts txt WHERE txt.ticket_id = t.id AND
        txt.created_at = (SELECT MAX(tt.created_at) FROM texts tt WHERE tt.ticket_id = t.id)) ttt) AS text FROM tickets t 
        INNER JOIN user_company uc ON t.client_id = uc.user_id WHERE t.status = 'active' AND uc.company_id = $1 AND t.type = $2
        ORDER BY read, deadline`, [req.payload.company, req.params.type]), (err, ticketList) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(ticketList.rows)
      })
    }
  }
}

const getFavorites = (req, res) => {
  if (req.params.type === 'all') {
    client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.type, t.favorite, t.created_at, t.read, t.user_id, t.deadline, t.status,
      (SELECT row_to_json(c) FROM (SELECT cli.id, cli.username, cli.name, cli.avatar FROM users cli WHERE cli.id = t.client_id) c) AS client,
      (SELECT row_to_json(ttt) FROM (SELECT txt.plain_text AS text, txt.created_at FROM texts txt WHERE txt.ticket_id = t.id AND
      txt.created_at = (SELECT MAX(tt.created_at) FROM texts tt WHERE tt.ticket_id = t.id)) ttt) AS text
      FROM tickets t INNER JOIN user_company uc ON t.client_id = uc.user_id WHERE t.favorite = true AND t.status = 'active'
      AND uc.company_id = $1`, [req.payload.company]), (err, ticketList) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
      } else res.status(200).send(ticketList.rows)
    })
  } else {
    client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.type, t.favorite, t.created_at, t.read, t.user_id, t.deadline, t.status,
      (SELECT row_to_json(c) FROM (SELECT cli.id, cli.username, cli.name, cli.avatar FROM users cli WHERE cli.id = t.client_id) c) AS client,
      (SELECT row_to_json(ttt) FROM (SELECT txt.plain_text AS text, txt.created_at FROM texts txt WHERE txt.ticket_id = t.id AND
      txt.created_at = (SELECT MAX(tt.created_at) FROM texts tt WHERE tt.ticket_id = t.id)) ttt) AS text
      FROM tickets t INNER JOIN user_company uc ON t.client_id = uc.user_id WHERE t.favorite = true AND t.status = 'active'
      AND t.type = $2 AND uc.company_id = $1`, [req.payload.company, req.params.type]), (err, ticketList) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
      } else res.status(200).send(ticketList.rows)
    })
  }
}

const getArchive = (req, res) => {
  if (req.payload.roleName === 'client') {
    client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.created_at, (SELECT txt.plain_text AS text FROM texts txt
      WHERE txt.ticket_id = t.id AND txt.created_at = (SELECT MAX(tt.created_at) FROM texts tt WHERE tt.ticket_id = t.id)) AS text
      FROM tickets t WHERE t.client_id = $1 AND t.status = 'archived'`, [req.payload.id]), (err, ticketList) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
      } else res.status(200).send(ticketList.rows)
    })
  } else {
    client.query(new Query(`SELECT t.id, t.title, t.ticket_number, t.type, t.favorite, t.created_at, t.read, t.user_id, t.deadline, t.status,
      (SELECT row_to_json(c) FROM (SELECT cli.id, cli.username, cli.name, cli.avatar FROM users cli WHERE cli.id = t.client_id) c) AS client,
      (SELECT row_to_json(ttt) FROM (SELECT txt.plain_text AS text, txt.created_at FROM texts txt WHERE txt.ticket_id = t.id AND
      txt.created_at = (SELECT MAX(tt.created_at) FROM texts tt WHERE tt.ticket_id = t.id)) ttt) AS text FROM tickets t 
      INNER JOIN user_company uc ON t.client_id = uc.user_id WHERE t.status = 'archived' AND uc.company_id = $1`,
      [req.payload.company]), (err, ticketList) => {
      if (err) {
        console.log(err)
        res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
      } else res.status(200).send(ticketList.rows)
    })
  }
}

const changeFav = (req, res) => {
  var ticketIds = []
  req.body.ids.forEach((item) => {
    ticketIds.push(item.id)
  })
  client.query(new Query(`UPDATE tickets SET favorite = $1 WHERE id = ANY ($2)`, [!req.body.value, ticketIds]))
  res.status(200).end()
}

const getOneTicket = (req, res) => {
  get(req.params.id, (err, ticket) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(ticket)
  })
}

const changeRead = (req, res) => {
  client.query(new Query(`UPDATE tickets SET read = $1 WHERE id = $2`, [true, req.body.id]))
  res.status(200).end()
}

const changeDeadline = (req, res) => {
  client.query(new Query(`UPDATE tickets SET deadline = $1 WHERE id = $2 RETURNING *`, [req.body.date, req.body.id]), (err, ticket) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      createJob(ticket.rows[0], req.payload.company, 'soon', 1800000)
      createJob(ticket.rows[0], req.payload.company, 'late', 0)
      res.status(200).end()
    }
  })
}

const get = (id, clback) => {
  client.query(new Query(`SELECT t.id, t.type, t.deadline, t.favorite, t.ticket_number, t.title, t.created_at, t.read, t.status,
    (SELECT row_to_json(us) FROM (SELECT u.username, u.id FROM users u WHERE u.id = t.user_id) us) AS user,
    (SELECT row_to_json(cli) FROM (SELECT c.name, c.email, c.avatar, c.id FROM users c WHERE c.id = t.client_id) cli) AS client,
    (SELECT array_to_json(array_agg(row_to_json(txt))) FROM (SELECT tx.text, tx.doc, tx.created_at, tx.type, tx.files, tu.id AS "userId",
    tu.name AS "userName", tu.avatar AS "userAvatar", (SELECT array_to_json(array_agg(row_to_json(ref)))
    FROM (SELECT ru.id, ru.name, ru.username FROM users ru WHERE ru.id = ANY(tx.ref_users)) ref) AS ref_users
    FROM texts tx LEFT JOIN users tu ON tx.user_id = tu.id WHERE tx.ticket_id = t.id
    ORDER BY tx.created_at) txt) AS texts FROM tickets t WHERE t.id = $1`, [id]), (err, ticket) => {
    if (err) clback(err)
    else clback(null, ticket.rows[0])
  })
}

const deleteTickets = (req, res) => {
  var ticketIds = req.body
  if (ticketIds[0] === 'all') {
    ticketIds.splice(0, 1)
  }
  client.query(new Query(`UPDATE tickets SET status = $1 WHERE id = ANY ($2)`, ['archived', ticketIds]))
  res.status(200).end()
}

const restoreTickets = (req, res) => {
  var ticketIds = req.body
  if (ticketIds[0] === 'all') {
    ticketIds.splice(0, 1)
  }
  client.query(new Query(`UPDATE tickets SET status = $1 WHERE id = ANY ($2)`, ['active', ticketIds]))
  res.status(200).end()
}

const deleteForever = (req, res) => {
  var ticketIds = req.body
  if (ticketIds[0] === 'all') {
    ticketIds.splice(0, 1)
  }
  client.query(new Query(`DELETE FROM history_staff WHERE ticket_id IN ($1)`, ticketIds))
  client.query(new Query(`DELETE FROM notifications WHERE ticket_id IN ($1)`, ticketIds))
  client.query(new Query(`DELETE FROM texts WHERE ticket_id IN ($1)`, ticketIds))
  client.query(new Query(`DELETE FROM tickets WHERE id IN ($1)`, ticketIds))
  res.status(200).end()
}

const changeTypeChecked = (req, res) => {
  var ticketIds = req.body.ids
  if (ticketIds[0] === 'all') {
    ticketIds.splice(0, 1)
  }
  if (req.body.type === 'income') {
    client.query(new Query(`UPDATE tickets SET income_date = $1 WHERE id = ANY ($2)`, [new Date(), ticketIds]))
    client.query(new Query(`SELECT * FROM tickets WHERE id = ANY ($1)`, [ticketIds]), (err, list) => {
      list.rows.forEach(item => {
        if (item.type === 'pending') {
          var date = moment().add(3, 'hours')
          client.query(new Query(`UPDATE tickets SET deadline = $1 WHERE id = $2 RETURNING *`, [date, item.id]), (err, newTickets) => {
            createJob(newTickets.rows[0], req.payload.company, 'soon', 1800000)
            createJob(newTickets.rows[0], req.payload.company, 'late', 0)
          })
        }
      })
    })
  }
  if (req.body.type === 'done') {
    client.query(new Query(`UPDATE tickets SET closed_date = $1 WHERE id = ANY ($2)`, [new Date(), ticketIds]))
  }
  client.query(new Query(`UPDATE tickets SET type = $1 WHERE id = ANY ($2)`, [req.body.type, ticketIds]))
  res.status(200).end()
}

const assignUserChecked = (req, res) => {
  client.query(new Query(`UPDATE tickets SET user_id = $1 WHERE id = $2`, [req.body.userId, req.body.id]))
  deleteKeywordNotif(req.body.id, 'keyword')
  res.status(200).end()
}

const getTicketsOfClient = (req, res) => {
  client.query(new Query(`SELECT id, title FROM tickets WHERE client_id = $1`, [req.params.id]), (err, result) => {
    res.status(200).send(result.rows)
  })
}

module.exports = {
  getTickets,
  getFavorites,
  changeFav,
  getOneTicket,
  changeRead,
  changeDeadline,
  deleteTickets,
  getArchive,
  restoreTickets,
  deleteForever,
  changeTypeChecked,
  assignUserChecked,
  get,
  getTicketsOfClient
}
