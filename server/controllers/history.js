import { Client, Query } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const getProfileHistory = (req, res) => {
  client.query(new Query(`SELECT hp.created_at, hp.old_value, hp.new_value, hp.text_one, hp.text_two,
    (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name FROM users u1 WHERE u1.id = hp.user_from) uf) AS "userFrom",
    (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = hp.user_to) ut) AS "userTo"
    FROM history_profile hp WHERE hp.user_to = $1 ORDER BY hp.created_at DESC LIMIT 20`, [req.params.id]), (err, historyList) => {
    if (err) {
      console.log(err)
      res.status(400).end()
    } else res.status(200).send(historyList.rows).end()
  })
}

const getStaffHistory = (req, res) => {
  if (req.payload.roleName === 'client') {
    var arr = ['archive', 'answer', 'move', 'restore', 'delete']
    client.query(new Query(`SELECT hs.created_at, hs.text, hs.type,
      (SELECT row_to_json(td) FROM (SELECT t.id, t.ticket_number FROM tickets t WHERE t.id = hs.ticket_id) td) AS ticket,
      (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name, u1.avatar FROM users u1 WHERE u1.id = hs.user_from) uf) AS "userFrom",
      (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = hs.user_to) ut) AS "userTo"
      FROM history_staff hs INNER JOIN tickets tt ON hs.ticket_id = tt.id WHERE tt.client_id = $1 AND
      hs.type IN ($2) ORDER BY hs.created_at DESC LIMIT 20`, [req.payload.id, arr]), (err, historyList) => {
      if (err) {
        console.log(err)
        res.status(400).end()
      } else res.status(200).send(historyList.rows).end()
    })
  } else {
    client.query(new Query(`SELECT hs.created_at, hs.text, hs.type,
      (SELECT row_to_json(td) FROM (SELECT t.id, t.ticket_number FROM tickets t WHERE t.id = hs.ticket_id) td) AS ticket,
      (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name, u1.avatar FROM users u1 WHERE u1.id = hs.user_from) uf) AS "userFrom",
      (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = hs.user_to) ut) AS "userTo"
      FROM history_staff hs INNER JOIN user_company uc ON hs.user_from = uc.user_id WHERE uc.company_id = $1 ORDER BY hs.created_at DESC
      LIMIT 20`,
      [req.payload.company]), (err, historyList) => {
      if (err) {
        console.log(err)
        res.status(400).end()
      } else res.status(200).send(historyList.rows).end()
    })
  }
}

module.exports = {
  getProfileHistory,
  getStaffHistory
}
