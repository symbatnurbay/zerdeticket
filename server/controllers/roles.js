import { Client, Query } from 'pg'
import async from 'async'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const getRoles = (req, res) => {
  get(req.payload.company, (err, result) => {
    if (err) {
      console.log(err)
      res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(result)
  })
}

const addRole = (req, res) => {
  client.query(new Query(`SELECT MAX(level) AS "maxLevel" FROM roles WHERE company_id = $1`, [req.payload.company]), (err, maxLevel) => {
    if (err) {
      console.log(err)
      res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      client.query(new Query(`INSERT INTO roles (name, code, delete, company_id, level) VALUES($1, $2, $3, $4, $5) RETURNING id, name, code, delete`,
        [req.body.name, req.body.code, true, req.payload.company, maxLevel.rows[0].maxLevel + 1]), (err, newRole) => {
        if (err) {
          console.log(err)
          res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          if (req.body.accesses) {
            var q = async.queue((item, done) => {
              client.query(new Query(`INSERT INTO role_access (role_id, access_role, action) VALUES($1, $2, $3)`, [newRole.rows[0].id, item.access_role, item.action]), (err) => {
                if (err) done(err)
                else done()
              })
            }, req.body.accesses.length)
            q.drain = () => {
              client.query(new Query(`SELECT r.id, r.name, r.code, r.delete, (SELECT array_to_json(array_agg(row_to_json(ra))) FROM
                (SELECT ra1.action, r1.name, ra1.access_role FROM role_access ra1 INNER JOIN roles r1 ON ra1.access_role = r1.id WHERE ra1.role_id=r.id) ra) AS accesses
                FROM roles r WHERE r.id = $1`, [newRole.rows[0].id]), (err, result) => {
                if (err) {
                  console.log(err)
                  res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                } else res.status(200).send(result.rows[0])
              })
            }
            q.push(req.body.accesses, (err) => {
              if (err) {
                console.log(err)
                res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
              }
            })
          } else {
            res.status(200).send(newRole.rows[0])
          }
        }
      })
    }
  })
}

const editRole = (req, res) => {
  client.query(new Query(`UPDATE roles SET name = $1, code = $2 WHERE id = $3`, [req.body.name, req.body.code, req.body.id]))
  client.query(new Query(`SELECT * FROM role_access WHERE role_id = $1`, [req.body.id]), (err, accesses) => {
    if (err) {
      console.log(err)
      res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      async.waterfall([
        async.apply(removeEditRole, { oldArr: accesses.rows, newArr: req.body.accesses }),
        async.apply(addEditRole, req.body.id)
      ], (err) => {
        if (err) {
          console.log(err)
          res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          res.status(200).end()
        }
      })
    }
  })
}

// удаление с базы доступ на действия (при редактировании)
const removeEditRole = (data, callback) => {
  var index
  var tempArr = data.newArr ? data.newArr.slice() : null
  if (data.oldArr && data.oldArr.length > 0) {
    var q = async.queue((item, done) => {
      index = data.newArr ? data.newArr.findIndex(x => x.access_role === item.access_role && x.action === item.action) : -1
      if (index > -1) {
        delete tempArr[index]
        done()
      } else {
        client.query(new Query(`DELETE FROM role_access WHERE id = $1`, [item.id]), (err) => {
          if (err) done(err)
          else done()
        })
      }
    }, data.oldArr.length)
    q.drain = () => {
      callback(null, tempArr)
    }
    q.push(data.oldArr, (err) => {
      if (err) callback(err)
    })
  } else callback(null, tempArr)
}

// добавление в базу доступ на действия (при редактировании)
const addEditRole = (id, arr, callback) => {
  if (arr && arr.length > 0) {
    var q = async.queue((item, done) => {
      if (item) {
        client.query(new Query(`INSERT INTO role_access (role_id, access_role, action) VALUES($1, $2, $3)`, [id, item.access_role, item.action]), (err) => {
          if (err) done(err)
          else done()
        })
      } else done()
    }, arr.length)
    q.drain = () => { callback(null) }
    q.push(arr, (err) => {
      if (err) callback(err)
    })
  } else callback(null)
}

const deleteRole = (req, res) => {
  if (req.body.role[0] === 'all') {
    req.body.role.splice(0, 1)
  }
  client.query(new Query(`UPDATE users SET role_id = $1 WHERE role_id = ANY($2)`, [req.body.newRole, req.body.role]), (err) => {
    if (err) {
      console.log(err)
      res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      client.query(new Query(`DELETE FROM role_access WHERE role_id = ANY($1) OR access_role = ANY($1)`, [req.body.role]), (err) => {
        if (err) {
          console.log(err)
          res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          client.query(new Query(`DELETE FROM roles WHERE id = ANY($1)`, [req.body.role]), (err) => {
            if (err) {
              console.log(err)
              res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else {
              get(req.payload.company, (err, result) => {
                if (err) {
                  console.log(err)
                  res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                } else res.status(200).send(result)
              })
            }
          })
        }
      })
    }
  })
}

const get = (company, callback) => {
  client.query(new Query(`SELECT r.id, r.name, r.code, r.delete, r.level, (SELECT array_to_json(array_agg(row_to_json(ra))) FROM
    (SELECT ra1.action, r1.name, ra1.access_role FROM role_access ra1 INNER JOIN roles r1 ON ra1.access_role = r1.id WHERE ra1.role_id=r.id) ra) AS accesses
    FROM roles r WHERE r.name NOT IN ('client', 'Председатель Правления') AND r.company_id = $1 ORDER BY r.level`, [company]), (err, result) => {
    if (err) {
      callback(err)
    } else callback(null, result.rows)
  })
}

const getPages = (req, res) => {
  var arr = []
  client.query(new Query(`SELECT code FROM roles WHERE id = $1`, [req.payload.role]), (err, roleName) => {
    if (err) {
      console.log(err)
      res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      if (roleName.rows[0].code === 'client') {
        res.status(200).end()
      } else if (roleName.rows[0].code === 'chairman') {
        arr = [{
          link: 'clients',
          name: 'clients',
          text: 'Клиенты'
        }, {
          link: 'roles',
          name: 'roles',
          text: 'Должности'
        }, {
          link: 'departments',
          name: 'departments',
          text: 'Департаменты'
        }]
        const q = client.query(new Query(`SELECT id, code, name FROM roles WHERE company_id = $1 AND code <> 'chairman' ORDER BY id`, [req.payload.company]))
        q.on('row', (row) => {
          arr.push({
            link: row.code,
            name: 'type',
            text: row.name
          })
        })
        q.on('end', () => {
          res.status(200).send({ arr, access: 'all' })
        })
      } else {
        client.query(new Query(`SELECT r.id, r.code, r.name, ra.action FROM roles r INNER JOIN role_access ra ON ra.access_role = r.id 
          WHERE ra.role_id = $1 ORDER BY id`, [req.payload.role]), (err, accessList) => {
          if (err) {
            console.log(err)
            res.status(500).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
          } else if (accessList.rowCount === 0) {
            res.status(200).send({ arr, access: [] })
          } else {
            accessList.rows.forEach(item => {
              if (item.action === 'list' && arr.findIndex(x => x.code === item.code) === -1) {
                arr.push({
                  link: item.code,
                  name: 'type',
                  text: item.name
                })
              }
            })
            res.status(200).send({ arr, access: accessList.rows })
          }
        })
      }
    }
  })
}

const moveRole = (req, res) => {
  async.forEachOf(req.body, (value, key, callback) => {
    client.query(new Query(`UPDATE roles SET level = $1 WHERE id = $2`, [2 + key, value.id]), (err) => {
      if (err) callback(err)
      else callback()
    })
  }, err => {
    if (err) {
      console.error(err)
      res.status(400).send({ message: 'Не удалось отсортировать должности, попробуйте позже' })
    } else res.status(200).end()
  })
}

const defaultRoles = (company, clback) => {
  client.query(new Query(`INSERT INTO roles (name, code, delete, company_id, level) VALUES ('Председатель Правления', 'chairman', $1, $2, 1), ('Заместитель Председателя Правления', 'vicechairman', $1, $2, 2),
    ('Директор Департамента', 'director', $1, $2, 3), ('Специалист', 'specialist', $1, $2, 4), ('Канцеляр', 'counselor', $1, $2, 5) RETURNING id`, [false, company]), (err, roleIds) => {
    if (err) clback(err)
    else {
      roleIds.rows.slice(1, roleIds.rowCount).forEach((item, index) => {
        if (roleIds.rows[index + 2]) {
          client.query(new Query(`INSERT INTO role_access (role_id, access_role, action) VALUES ($1, $2, 'list'), ($1, $2, 'add'),
            ($1, $2, 'edit'), ($1, $2, 'delete')`, [item.id, roleIds.rows[index + 2].id]))
        }
      })
      clback(null, roleIds.rows[0].id)
    }
  })
}

module.exports = {
  getRoles,
  addRole,
  editRole,
  deleteRole,
  getPages,
  moveRole,
  defaultRoles
}
