import { Client, Query } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const getTodayStatistics = (req, res) => {
  client.query(new Query(`SELECT count(*) FROM tickets WHERE type = 'income' AND status = 'active' AND date(income_date) = current_date 
    AND client_id = ANY (SELECT user_id FROM user_company WHERE company_id = $1)`, [req.payload.company]), (err, resIncome) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось получить данные' })
    } else {
      client.query(new Query(`SELECT count(*) FROM tickets WHERE type = 'done' AND status = 'active' AND date(closed_date) = current_date 
        AND client_id = ANY (SELECT user_id FROM user_company WHERE company_id = $1)`, [req.payload.company]), (err, resDone) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Не удалось получить данные' })
        } else {
          res.status(200).send({ todayClosed: resDone.rows[0].count, todayIncome: resIncome.rows[0].count })
        }
      })
    }
  })
}

const getTodayDone = (req, res) => {
  client.query(new Query(`SELECT COUNT(*), to_timestamp(FLOOR((EXTRACT('epoch' FROM closed_date) / 18000 )) * 18000) AS interval_alias
    FROM tickets WHERE date(closed_date) = current_date AND client_id = ANY (SELECT user_id FROM user_company WHERE company_id = $1)
    GROUP BY interval_alias ORDER BY interval_alias`, [req.payload.company]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось получить данные' })
    } else {
      res.status(200).send(result.rows)
    }
  })
}

const getOngoing = (req, res) => {
  client.query(new Query(`SELECT t.id, t.title, t.created_at, t.ticket_number,
    (SELECT row_to_json(c) FROM (SELECT cli.id, cli.username, cli.name FROM users cli WHERE cli.id = t.client_id) c) AS client,
    (SELECT row_to_json(u) FROM (SELECT us.id, us.username, us.name, us.avatar FROM users us WHERE us.id = t.user_id) u) AS user
    FROM tickets t WHERE t.client_id = ANY (SELECT user_id FROM user_company WHERE company_id = $1) AND t.type = 'ongoing'
    AND t.status = 'active' ORDER BY t.income_date DESC`, [req.payload.company]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось получить данные' })
    } else {
      res.status(200).send(result.rows)
    }
  })
}

const getDeadline = (req, res) => {
  client.query(new Query(`SELECT t.id, t.ticket_number, t.deadline,
    (SELECT row_to_json(u) FROM (SELECT us.id, us.username, us.name, us.avatar FROM users us WHERE us.id = t.user_id) u) AS user
    FROM tickets t WHERE t.client_id = ANY (SELECT user_id FROM user_company WHERE company_id = $1) AND t.status = 'active'
    AND t.deadline < current_timestamp AND t.closed_date IS NULL ORDER BY t.deadline DESC`,
    [req.payload.company]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось получить данные' })
    } else {
      res.status(200).send(result.rows)
    }
  })
}

const getAllTickets = (req, res) => {
  client.query(new Query(`SELECT COUNT(*), type FROM tickets WHERE status = 'active' AND
    client_id = ANY (SELECT user_id FROM user_company WHERE company_id = $1) GROUP BY type`, [req.payload.company]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось получить данные' })
    } else {
      res.status(200).send(result.rows)
    }
  })
}

const getThisMonth = (req, res) => {
  client.query(new Query(`SELECT count(*), date_part('day', closed_date) day_of_month FROM tickets WHERE type = 'done' AND status = 'active'
    AND date_part('month', closed_date) = $2 AND date_part('year', closed_date) = date_part('year', current_date)
    AND client_id = ANY (SELECT user_id FROM user_company WHERE company_id = $1) GROUP BY day_of_month`,
    [req.payload.company, parseInt(req.params.month) + 1]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Не удалось получить данные' })
    } else {
      res.status(200).send(result.rows)
    }
  })
}

module.exports = {
  getTodayStatistics,
  getTodayDone,
  getOngoing,
  getDeadline,
  getAllTickets,
  getThisMonth
}
