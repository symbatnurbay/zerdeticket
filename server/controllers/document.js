import { Client, Query } from 'pg'
import moment from 'moment'
import pdf from 'html-pdf'
import { createJob } from './jobProcesses'
import { get } from './ticket'
import { findUsers } from './keyword'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const addTicket = (req, res) => {
  var date = moment().add(3, 'hours')
  pdf.create(req.body.text, { width: (595 * (72 / 96)) + 'px', timeout: 30000, border: {
    top: '2cm',
    right: '1.5cm',
    bottom: '2cm',
    left: '1.5cm'
  }}).toFile(`./static/files/tickets/${req.body.title + Date.now()}.pdf`, (err, r) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      var temp = r.filename.split('/')
      var doc = {
        path: `/files/tickets/${temp[temp.length - 1]}`,
        name: req.body.title
      }
      var files = req.files ? req.files.map(file => {
        return {
          path: `/files/${file.filename}`,
          name: file.originalname
        }
      }) : null

      client.query(new Query(`SELECT t.id FROM tickets t INNER JOIN user_company uc ON t.client_id = uc.user_id WHERE uc.company_id = $1`, [req.payload.company]), (err, result) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          client.query(new Query(`INSERT INTO tickets (title, ticket_number, type, favorite, status, deadline, created_at, read, client_id, income_date)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $7) RETURNING *`, [req.body.title, result.rowCount + 1, req.body.type, false, 'active', date,
              new Date(), false, req.payload.id]), (err, newTicket) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else {
              client.query(new Query(`INSERT INTO texts (text, type, created_at, files, user_id, ticket_id, plain_text, doc)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`, [req.body.text, 'ticket', new Date(), JSON.stringify(files), req.payload.id,
                  newTicket.rows[0].id, req.body.plainText, JSON.stringify(doc)]), (err) => {
                if (err) {
                  console.log(err)
                  res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                } else {
                  if (req.body.type !== 'pending') {
                    findUsers(req.body.plainText, newTicket.rows[0].id)
                    createJob(newTicket.rows[0], req.payload.company, 'soon', 1800000)
                    createJob(newTicket.rows[0], req.payload.company, 'late', 0)
                    createJob(newTicket.rows[0], req.payload.company, 'nobody', 86400000)
                  }
                  res.status(200).send({ id: newTicket.rows[0].id })
                }
              })
            }
          })
        }
      })
    }
  })
}

const addAnswer = (req, res) => {
  var answer = {
    type: req.payload.roleName === 'client' ? 'ticket' : req.body.answerType,
    text: req.body.answerType === 'note' ? req.body.note : req.body.text,
    files: req.files ? req.files.map(file => {
      return {
        path: `/files/${file.filename}`,
        name: file.originalname
      }
    }) : null,
    user: req.payload.roleName !== 'client' ? req.payload.id : null,
    refUsers: req.body.refUsers ? req.body.refUsers.map(item => { return parseInt(item, 10) }) : null
  }
  const ticketUser = !JSON.parse(req.body.user) || JSON.parse(req.body.user).id === 'user1' || !JSON.parse(req.body.user).id ? null
    : JSON.parse(req.body.user).id
  client.query(new Query(`UPDATE tickets SET read = false, user_id = $1, type = $2 WHERE id = $3`, [ticketUser, req.body.type, req.params.id]))
  client.query(new Query(`INSERT INTO texts (text, type, created_at, files, user_id, ref_users, ticket_id, plain_text) VALUES ($1, $2, $3, $4, $5, $6, $7, $1)`,
    [answer.text, answer.type, new Date(), JSON.stringify(answer.files), answer.user, answer.refUsers, req.params.id]), (err) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      get(req.params.id, (err, ticket) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else res.status(200).send(ticket)
      })
    }
  })
}

module.exports = {
  addTicket,
  addAnswer
}
