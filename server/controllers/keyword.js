import { Client, Query } from 'pg'
import async from 'async'
import { checkTelegramAcc } from './telegram'
import Configs from '../config/config'

const socketio = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379 })
const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const addKeyword = (keywords, depId) => {
  var q = async.queue((item, done) => {
    client.query(new Query(`SELECT id FROM keywords WHERE UPPER(word) = UPPER($1)`, [item.trim()]), (err, id) => {
      if (err) done(err)
      else if (id.rowCount === 0) {
        client.query(new Query(`INSERT INTO keywords (word, department_id) VALUES ($1, $2) RETURNING id`,
          [item.trim(), depId]), (err, newWord) => {
          if (err) done(err)
          else done(null)
        })
      } else done(null)
    })
  }, keywords.length)
  q.drain = () => {
    console.log('keywords added')
  }
  q.push(keywords, (err) => {
    if (err) throw new Error(err)
  })
}

const findUsers = (text, ticket) => {
  client.query(new Query(`SELECT * FROM keywords`), (err, keywords) => {
    if (err) throw new Error(err)
    else {
      async.forEachOf(keywords.rows, (value, key, callback) => {
        if (text.toUpperCase().indexOf(value.word.toUpperCase()) > -1) {
          client.query(new Query(`SELECT user_id FROM user_department WHERE department_id = $1 AND director = true`,
            [value.department_id]), (err, result) => {
            if (err) callback(err)
            else if (result.rowCount > 0) {
              result.rows.forEach(item => {
                client.query(new Query(`SELECT id FROM notifications WHERE user_to = $1 AND type = 'keyword'`, [item.user_id]), (err, r) => {
                  if (err) callback(err)
                  else {
                    if (r.rowCount > 0) {
                      client.query(new Query(`DELETE FROM notifications WHERE id = $1`, [r.rows[0].id]))
                    }
                    client.query(new Query(`INSERT INTO notifications (user_to, created_at, type, ticket_id, read) VALUES ($1, $2, $3, $4, $5)
                      RETURNING *`, [item.user_id, new Date(), 'keyword', ticket, false]), (err, notification) => {
                      if (err) callback(err)
                      else {
                        var room = 'user-' + item.user_id
                        checkTelegramAcc(item.user_id, notification.rows[0])
                        socketio.to(room).emit('newTicketNotification', notification.rows[0])
                      }
                    })
                  }
                })
              })
            }
          })
        }
      }, err => {
        if (err) console.error(err)
      })
    }
  })
}

module.exports = {
  addKeyword,
  findUsers
}
