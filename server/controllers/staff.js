import { Client, Query } from 'pg'
import { add } from './clients'
import { addKeyword } from './keyword'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const getStaff = (req, res) => {
  client.query(new Query(`SELECT u.id, u.name, u.email, u.username, u.avatar, d.name AS department, b.name AS boss, r.level
    FROM users u INNER JOIN roles r ON u.role_id = r.id LEFT JOIN user_department ud ON ud.user_id = u.id
    LEFT JOIN departments d ON ud.department_id = d.id LEFT JOIN boss_emp be ON be.emp_id = u.id
    LEFT JOIN users b ON b.id = be.boss_id WHERE r.code = $1 AND u.status = $2`, [req.params.type, 'active']), (err, staffList) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      client.query(new Query(`SELECT level FROM roles WHERE code = 'director' AND company_id = $1`, [req.payload.company]), (err, levelOfDir) => {
        client.query(new Query(`SELECT level FROM roles WHERE code = $2 AND company_id = $1`, [req.payload.company, req.params.type]), (err, level) => {
          res.status(200).send({
            list: staffList.rows,
            levelOfDir: levelOfDir.rows[0].level,
            level: level.rows[0].level }).end()
        })
      })
    }
  })
}

const getBosslist = (req, res) => {
  client.query(new Query(`SELECT u.id, u.name FROM users u INNER JOIN roles r ON u.role_id = r.id WHERE UPPER(u.name) LIKE UPPER($1) AND r.code NOT IN ($2, 'client')
    AND u.id NOT IN (SELECT user_id FROM user_department WHERE director = $3) AND u.status = $4`, [`%${req.params.value}%`, req.params.type, false, 'active']), (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(list.rows).end()
  })
}

const getEmplist = (req, res) => {
  client.query(new Query(`SELECT id, name FROM users WHERE UPPER(name) LIKE UPPER($1) AND id = ANY (SELECT user_id FROM user_department
    WHERE director = $2 AND department_id = $3) AND status = $4`, [`%${req.params.value}%`, false, req.params.department, 'active']), (err, list) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(list.rows).end()
  })
}

const addStaff = (req, res) => {
  client.query(new Query(`SELECT id FROM users WHERE username = $1`, [req.body.username]), (err, clientUsername) => {
    if (clientUsername.rowCount > 0) {
      res.status(403).send({ message: 'Пользователь с таким логином уже существует', origin: 'username' }).end()
    } else {
      client.query(new Query(`SELECT id FROM users WHERE email = $1`, [req.body.email]), (err, clientEmail) => {
        if (clientEmail.rowCount > 0) {
          res.status(403).send({ message: 'Пользователь с таким email-м уже существует', origin: 'email' }).end()
        } else {
          client.query(new Query(`SELECT id, level FROM roles WHERE code = $1 AND company_id = $2`, [req.params.type, req.payload.company]), (err, roleId) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else {
              add(req.body, req.payload.company, roleId.rows[0].id, (err, newUser) => {
                if (err) {
                  console.log(err)
                  res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                } else {
                  if (req.body.boss && req.body.boss.id) {
                    client.query(new Query(`INSERT INTO boss_emp (boss_id, emp_id) VALUES ($1, $2)`, [req.body.boss.id, newUser.id]))
                  }
                  if (req.body.department && req.body.department.id) {
                    if (req.params.type === 'director') {
                      client.query(new Query(`UPDATE user_department SET director = false WHERE department_id = $1 AND director = true RETURNING user_id`, [req.body.department.id]), (err, oldDirector) => {
                        if (err) {
                          console.log(err)
                          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
                        } else if (oldDirector.rowCount > 0) {
                          client.query(new Query(`UPDATE users SET role_id = (SELECT id FROM roles WHERE level = $1) WHERE id = $2`, [roleId.rows[0].level + 1, oldDirector.rows[0].user_id]))
                          client.query(new Query(`DELETE FROM boss_emp WHERE emp_id = $1`, [oldDirector.rows[0].user_id]))
                        }
                      })
                    }
                    client.query(new Query(`INSERT INTO user_department (user_id, department_id, director) VALUES ($1, $2, $3)`, [newUser.id, req.body.department.id, (req.params.type === 'director')]))
                  }
                  newUser.department = req.body.department ? req.body.department.name : null
                  newUser.boss = req.body.boss ? req.body.boss.name : null
                  client.query(new Query(`SELECT level FROM roles WHERE id = $1`, [roleId.rows[0].id]), (err, level) => {
                    newUser.level = level.rows[0].level
                    res.status(200).send({ user: newUser, message: 'Сотрудник успешно добавлен' })
                  })
                }
              })
            }
          })
        }
      })
    }
  })
}

const checkStatus = (req, res) => {
  client.query(new Query(`SELECT ud.*, d.name FROM user_department ud INNER JOIN departments d ON ud.department_id = d.id
    WHERE ud.user_id = $1 AND director = $2`, [req.params.id, true]), (err, director) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else if (director.rowCount === 0) {
      client.query(new Query(`SELECT id FROM boss_emp WHERE boss_id = $1`, [req.params.id]), (err, boss) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else if (boss.rowCount === 0) res.status(200).end()
        else res.status(200).send({ role: 'boss' })
      })
    } else res.status(200).send({ role: 'director', data: director.rows[0] })
  })
}

const deleteStaff = (req, res) => {
  if (!req.body.newBoss) {
    client.query(new Query(`UPDATE users SET status = $1 WHERE id = $2`, ['archived', req.body.id]))
    res.status(200).end()
  } else if (req.body.newBoss.role === 'director') {
    client.query(new Query(`UPDATE user_department SET director = $1 WHERE user_id = $2 AND department_id = $3`,
      [false, req.body.id, req.body.newBoss.department]))
    client.query(new Query(`UPDATE user_department SET director = $1 WHERE user_id = $2 AND department_id = $3`,
      [true, req.body.newBoss.id, req.body.newBoss.department]))
    client.query(new Query(`SELECT id FROM roles WHERE code = 'director' AND company_id = $1`, [req.payload.company]), (err, directorId) => {
      client.query(new Query(`UPDATE users SET role_id = $1 WHERE id = $2`, [directorId.rows[0].id, req.body.newBoss.id]))
      client.query(new Query(`SELECT id FROM roles WHERE code = 'specialist' AND company_id = $1`, [req.payload.company]), (err, specId) => {
        client.query(new Query(`UPDATE users SET role_id = $1, status = 'archived' WHERE id = $2`, [specId.rows[0].id, req.body.id]))
        res.status(200).end()
      })
    })
  } else {
    client.query(new Query(`UPDATE users SET status = $1 WHERE id = $2`, ['archived', req.body.id]))
    client.query(new Query(`UPDATE boss_emp SET boss_id = $1 WHERE boss_id = $2`, [req.body.newBoss.id, req.body.id]))
    res.status(200).end()
  }
}

const getOne = (req, res) => {
  client.query(new Query(`SELECT u.id, u.name, u.username, u.email, u.avatar, (SELECT row_to_json(r1) FROM (SELECT r.id, r.name, r.level, r.code FROM roles r WHERE r.id = u.role_id) r1) AS role,
    (SELECT row_to_json(d1) FROM (SELECT d.id, d.name, ud.director FROM departments d RIGHT JOIN user_department ud ON ud.department_id = d.id WHERE ud.user_id = $1) d1) AS department,
    (SELECT row_to_json(b1) FROM (SELECT b.id, b.name FROM users b RIGHT JOIN boss_emp be ON be.boss_id = b.id WHERE be.emp_id = $1) b1) AS boss,
    (SELECT array_to_json(array_agg(row_to_json(k1))) FROM (SELECT k.* FROM keywords k RIGHT JOIN user_department uk ON uk.department_id = k.department_id WHERE uk.user_id = $1) k1) AS keywords
    FROM users u WHERE u.id = $1`, [req.params.id]), (err, user) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else {
      client.query(new Query(`SELECT id, name, code, level FROM roles WHERE company_id = $1 AND code NOT IN ('client', 'chairman') ORDER BY level`, [req.payload.company]), (err, roles) => {
        if (err) {
          console.log(err)
          res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
        } else {
          client.query(new Query(`SELECT id, name FROM departments WHERE company_id = $1`, [req.payload.company]), (err, departments) => {
            if (err) {
              console.log(err)
              res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
            } else {
              res.status(200).send({ user: user.rows[0], roles: roles.rows, departments: departments.rows }).end()
            }
          })
        }
      })
    }
  })
}

const updateOne = (req, res) => {
  if (req.body.name) {
    client.query(new Query(`UPDATE users SET name = $1 WHERE id = $2`, [req.body.name, req.params.id]))
  }
  if (req.body.keywords) {
    const keywords = req.body.keywords.split(',')
    addKeyword(keywords, req.body.department.id)
  }
  client.query(new Query(`SELECT (SELECT row_to_json(r1) FROM (SELECT r.id, r.name, r.level, r.code FROM roles r WHERE r.id = u.role_id) r1) AS role,
    (SELECT row_to_json(d1) FROM (SELECT d.id, d.name, ud.director FROM departments d RIGHT JOIN user_department ud ON ud.department_id = d.id WHERE ud.user_id = $1) d1) AS department,
    (SELECT row_to_json(b1) FROM (SELECT b.id, b.name FROM users b RIGHT JOIN boss_emp be ON be.boss_id = b.id WHERE be.emp_id = $1) b1) AS boss
    FROM users u WHERE u.id = $1`, [req.params.id]), (err, user) => {
    if (req.body.boss.id) {
      if (!user.rows[0].boss) {
        client.query(new Query(`INSERT INTO boss_emp (boss_id, emp_id) VALUES ($1, $2)`, [req.body.boss.id, req.params.id]))
      } else if (req.body.boss.id !== user.rows[0].boss.id) {
        client.query(new Query(`UPDATE boss_emp SET boss_id = $1 WHERE emp_id = $2`, [req.body.boss.id, req.params.id]))
      }
    }
    if (req.body.department.id && user.rows[0].department && req.body.department.id !== user.rows[0].department.id) {
      client.query(new Query(`UPDATE user_department SET department_id = $1 WHERE user_id = $2`, [req.body.department.id, req.params.id]))
    }
    if (req.body.role.id !== user.rows[0].role.id) {
      client.query(new Query(`SELECT id, level FROM roles WHERE code = 'director' AND company_id = $1`, [req.payload.company]), (err, roleLevel) => {
        const levelOfDir = roleLevel.rows[0].level
        if (req.params.type === 'director') {
          if (req.body.role.level > levelOfDir) {
            client.query(new Query(`DELETE FROM boss_emp WHERE emp_id = $1`, [req.params.id]))
            client.query(new Query(`UPDATE user_department SET director = $1 WHERE user_id = $2`, [false, req.params.id]))
          } else if (req.body.role.level < levelOfDir) {
            client.query(new Query(`DELETE FROM user_department WHERE user_id = $1`, [req.params.id]))
          }
          client.query(new Query(`INSERT INTO boss_emp (boss_id, emp_id) VALUES ($1, $2)`, [req.body.boss.id, req.body.director.id]))
          client.query(new Query(`UPDATE user_department SET director = $1 WHERE user_id = $2`, [true, req.body.director.id]))
          client.query(new Query(`UPDATE users SET role_id = $1 WHERE id = $2`, [roleLevel.rows[0].id, req.body.director.id]))
        } else {
          if (req.body.role.level > levelOfDir && user.rows[0].role.level < levelOfDir) {
            client.query(new Query(`DELETE FROM boss_emp WHERE emp_id = $1`, [req.params.id]))
            client.query(new Query(`UPDATE boss_emp SET boss_id = $1 WHERE boss_id = $2`, [user.rows[0].boss.id, req.params.id]))
            client.query(new Query(`INSERT INTO user_department (user_id, department_id, director) SELECT $1, $2, $3 WHERE NOT EXISTS 
              (SELECT id FROM user_department WHERE user_id = $1 AND department_id = $2)`, [req.params.id, req.body.department.id, false]))
          } else if (req.body.role.level < levelOfDir && user.rows[0].role.level > levelOfDir) {
            client.query(new Query(`INSERT INTO boss_emp (boss_id, emp_id) SELECT $1, $2 WHERE NOT EXISTS (SELECT id FROM boss_emp
              WHERE emp_id = $2 AND boss_id = $1)`, [req.body.boss.id, req.params.id]))
            client.query(new Query(`DELETE FROM user_department WHERE user_id = $1`, [req.params.id]))
          } else if (req.body.role.level === levelOfDir) {
            client.query(new Query(`UPDATE user_department SET director = $1 WHERE department_id = $2 AND director = $3 RETURNING user_id`,
              [false, req.body.department.id, true]), (err, directorId) => {
              if (err) console.log(err)
              else {
                client.query(new Query(`SELECT id FROM roles WHERE code = 'specialist' AND company_id = $1`, [req.payload.company]), (err, roleId) => {
                  if (err) console.log(err)
                  else {
                    client.query(new Query(`UPDATE users SET role_id = $1 WHERE id = $2`, [roleId.rows[0].id, directorId.rows[0].user_id]))
                  }
                })
              }
            })
            if (user.rows[0].role.level > levelOfDir) {
              client.query(new Query(`UPDATE user_department SET director = $1 WHERE user_id = $2`, [true, req.params.id]))
            } else {
              client.query(new Query(`UPDATE boss_emp SET boss_id = $1 WHERE boss_id = $2`, [req.body.boss.id, req.params.id]))
              client.query(new Query(`INSERT INTO user_department (user_id, department_id, director) SELECT $1, $2, $3 WHERE NOT EXISTS
                (SELECT id FROM user_department WHERE user_id = $1 AND department_id = $2)`, [req.params.id, req.body.department.id, true]))
            }
          }
        }
        client.query(new Query(`UPDATE users SET role_id = $1 WHERE id = $2`, [req.body.role.id, req.params.id]))
      })
    }
    res.status(200).send({ message: 'Данные успешно обновлены' })
  })
}

module.exports = {
  getStaff,
  getBosslist,
  addStaff,
  checkStatus,
  deleteStaff,
  getEmplist,
  getOne,
  updateOne
}
