import TelegramBot from 'node-telegram-bot-api'
import { Client, Query } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
const socketio = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379 })
const token = Configs.telegram.token
client.connect()

const bot = new TelegramBot(token, { polling: false })

bot.on('polling_error', (err) => {
  console.log(err)
})

bot.on('webhook_error', (error) => {
  console.log(error.code, 'bot wh') // => 'EPARSE'
})

bot.onText(/\/start/, ({ from }) => {
  bot.sendMessage(from.id, `Добро пожаловать в телеграмм бот ZERDE Ticket. Для продолжения работы с ботом напишите команду /username и ваше имя пользователя в системе. Пример: /username myUsername`)
})

bot.onText(/\/username (.+)/, (msg, match) => {
  client.query(new Query(`SELECT id FROM telegram_accounts WHERE user_id = (SELECT id FROM users WHERE username = $1)`, [match[1]]), (err, user) => {
    if (err) {
      console.log(err)
      bot.sendMessage(msg.from.id, `Неизвестная ошибка. Повторите попытку позже.`)
    } else if (user.rowCount === 0) {
      bot.sendMessage(msg.from.id, `Пользователь не найден. Логин неправильный или вы не нажимали Подключить в настройках системы.`)
    } else {
      client.query(new Query(`UPDATE telegram_accounts SET telegram_id = $1, first_name = $2, last_name = $3, username = $4 WHERE id = $5`,
        [msg.from.id, msg.from.first_name, msg.from.last_name, msg.from.username, user.rows[0].id]))
      bot.sendMessage(msg.from.id, `Введите секретный код полученный в системе через команду /code. Пример: /code myCode`)
    }
  })
})

bot.onText(/\/code (.+)/, (msg, match) => {
  client.query(new Query(`SELECT id, code FROM telegram_accounts WHERE telegram_id = $1`, [msg.from.id]), (err, user) => {
    if (err) {
      console.log(err)
      bot.sendMessage(msg.from.id, `Неизвестная ошибка. Повторите попытку позже.`)
    } else if (user.rowCount === 0) {
      bot.sendMessage(msg.from.id, `Пользователь не найден. Логин неправильный или вы не нажимали Подключить в настройках системы.`)
    } else if (user.rows[0].code === match[1]) {
      client.query(new Query(`UPDATE telegram_accounts SET active = true, code = $2 WHERE id = $1 RETURNING *`,
        [user.rows[0].id, null]), (err, result) => {
        if (err) console.log(err)
        else {
          var room = 'user-' + result.rows[0].user_id
          socketio.to(room).emit('telegramAccountAdded', result.rows[0])
          bot.sendMessage(msg.from.id, `Вы успешно подключили телеграм аккаунт. Теперь все ваши уведомления в системе будут приходить в этот бот.`)
        }
      })
    } else {
      bot.sendMessage(msg.from.id, `Код неправильный.`)
    }
  })
})

const sendMsg = (id, msg) => {
  bot.sendMessage(id, msg)
}

module.exports = {
  bot,
  sendMsg
}
