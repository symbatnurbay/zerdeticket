import { Client, Query } from 'pg'
import Configs from '../config/config'

const socketio = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379 })
const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const deleteNotification = (req, res) => {
  client.query(new Query(`DELETE FROM notifications WHERE id = $1`, [req.body.id]))
  res.status(200).end()
}

const checkNewTicket = (req, res) => {
  client.query(new Query(`SELECT * FROM notifications WHERE user_to = $1 AND type = 'keyword'`, [req.payload.id]), (err, notification) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, попробуйте позже' }).end()
    } else res.status(200).send(notification.rows[0]).end()
  })
}

const deleteKeywordNotif = (ticketId, type) => {
  client.query(new Query(`DELETE FROM notifications WHERE ticket_id = $1 AND type = $2`, [ticketId, type]))
}

const getNotifications = (req, res) => {
  client.query(new Query(`SELECT nf.id, nf.created_at, nf.text, nf.type, nf.read,
    (SELECT row_to_json(td) FROM (SELECT t.id, t.ticket_number FROM tickets t WHERE t.id = nf.ticket_id) td) AS ticket,
    (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name, u1.avatar FROM users u1 WHERE u1.id = nf.user_from) uf) AS "userFrom",
    (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = nf.user_to) ut) AS "userTo"
    FROM notifications nf WHERE nf.user_to = $1 AND type <> 'keyword' ORDER BY nf.created_at DESC`, [req.payload.id]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, повторите позже' })
    } else res.status(200).send(result.rows)
  })
}

const changeUnread = (req, res) => {
  client.query(new Query(`UPDATE notifications SET read = true WHERE id = ANY ($1)`, [req.body]))
  res.status(200).end()
}

const addNotification = (userTo, data, type) => {
  var textArr = type === 'soon' ? ['До истечения дедлайна протокола', 'осталось 30 минут']
    : type === 'late' ? ['Истек срок выполнения протокола'] : ['Ответственный не выбран для протокола']
  client.query(new Query(`INSERT INTO notifications (user_to, created_at, type, ticket_id, text, read) VALUES ($1, $2, $3, $4, $5, $6)
    RETURNING id`, [userTo, new Date(), type, data.ticket, textArr, false]), (err, notifId) => {
    if (err) console.error(err)

    client.query(new Query(`SELECT nf.id, nf.created_at, nf.text, nf.type, nf.read,
      (SELECT row_to_json(td) FROM (SELECT t.id, t.ticket_number FROM tickets t WHERE t.id = nf.ticket_id) td) AS ticket,
      (SELECT row_to_json(uf) FROM (SELECT u1.id, u1.username, u1.name, u1.avatar FROM users u1 WHERE u1.id = nf.user_from) uf) AS "userFrom",
      (SELECT row_to_json(ut) FROM (SELECT u2.id, u2.username, u2.name FROM users u2 WHERE u2.id = nf.user_to) ut) AS "userTo"
      FROM notifications nf WHERE nf.id = $1`, [notifId.rows[0].id]), (err, notif) => {
      if (err) console.error(err)

      console.log(`notification added: type ${type}`)
      require('./telegram').checkTelegramAcc(userTo, notif.rows[0])
      socketio.to(`user-${userTo}`).emit('notificationAdded', notif.rows[0])
    })
  })
}

module.exports = {
  deleteNotification,
  checkNewTicket,
  deleteKeywordNotif,
  getNotifications,
  changeUnread,
  addNotification
}
