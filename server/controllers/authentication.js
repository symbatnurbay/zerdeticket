import passport from 'passport'
import { Client, Query } from 'pg'
import mailsender from '../utils/mail'
import Configs from '../config/config'
import { setPassword, generateJwt } from '../utils/personal'
import { defaultRoles } from './roles'
import { addDepartments, addTemplate } from './others'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const signup = (req, res) => {
  client.query(new Query(`SELECT id FROM users WHERE username = $1 OR email = $2`, [req.body.username, req.body.email]), (err, result) => {
    if (err) {
      console.log(err)
      return res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
    } else {
      if (result.rowCount > 0) {
        return res.status(403).send({ message: 'Пользователь с таким логином или email-м существует', origin: 'username' })
      } else {
        var password = setPassword(req.body.password)

        client.query(new Query('INSERT INTO companies (email, name) VALUES ($1, $2) RETURNING id', [req.body.email, req.body.company]), (err, company) => {
          if (err) {
            console.log(err)
            return res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
          } else {
            defaultRoles(company.rows[0].id, (err, data) => {
              if (err) {
                console.log(err)
                res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
              } else {
                client.query(new Query(`INSERT INTO users (name, email, username, status, avatar, hash, salt, role_id, created_at)
                  VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`, [req.body.name, req.body.email, req.body.username, 'active', '/images/photo-placeholder.png',
                    password.hash, password.salt, data, new Date()]), (err, user) => {
                  if (err) {
                    console.log(err)
                    res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
                  } else {
                    client.query(`INSERT INTO user_company (user_id, company_id) VALUES ($1, $2)`, [user.rows[0].id, company.rows[0].id])
                    addDepartments(company.rows[0].id)
                    addTemplate(company.rows[0].id)
                    var newUser = user.rows[0]
                    newUser.company_id = company.rows[0].id
                    newUser.roleName = 'chairman'
                    const token = generateJwt(newUser)
                    res.status(200).send({ token })
                  }
                })
              }
            })
          }
        })
      }
    }
  })
}

const login = (req, res) => {
  passport.authenticate('local', (err, user, info) => {
    let token
    if (err) {
      console.log(err)
      return res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
    }
    if (!user) {
      return res.status(404).send(info)
    }
    if (user) {
      token = generateJwt(user)
      res.status(200).send({ token })
    }
  })(req, res)
}

const forgot = (req, res) => {
  client.query(new Query(`SELECT * FROM users WHERE email = $1`, [req.body.email]), (err, result) => {
    if (err) {
      console.log(err)
      return res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
    } else if (result.rowCount === 0) {
      return res.status(403).send({ message: 'Такой email не зарегистрирован в системе', origin: 'email' })
    } else {
      const link = `http://${Configs.serverData.addressProd}/#/newpass?user=${result.rows[0].email}&expiry=${Date.now()}`
      const emailText = `Здравствуйте, ${result.rows[0].name} <br><br> В системе ZERDE Ticket (zerde-ticket.tk) была запрошена информация для смены вашего пароля. <br><br> Для изменения пароля, пожалуйста, перейдите по ссылке:
${link} <br><br> С уважением, команда проекта ZERDE Ticket`
      mailsender.sendMail(result.rows[0].email, 'Смена пароля ZERDE Ticket', emailText, emailText, (err, message) => {
        if (err) return res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
        else res.status(200).end()
      })
    }
  })
}

const check = (req, res) => {
  client.query(new Query(`SELECT id FROM users WHERE email = $1`, [req.body.email]), (err, result) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
    } else if (result.rowCount === 0) {
      res.status(404).send({ message: 'Пользователь не найден' })
    } else {
      res.status(200).end()
    }
  })
}

const change = (req, res) => {
  var password = setPassword(req.body.pass)
  client.query(new Query(`UPDATE users SET hash = $1, salt = $2 WHERE email = $3`, [password.hash, password.salt, req.body.email]), (err) => {
    if (err) {
      console.log(err)
      res.status(400).send({ message: 'Неизвестная ошибка, повторите попытку позже' })
    } else {
      res.status(200).end()
    }
  })
}

module.exports = {
  signup,
  login,
  forgot,
  check,
  change
}
