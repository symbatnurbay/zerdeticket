import kue from 'kue'
import { Client, Query } from 'pg'
import Configs from '../config/config'

const client = new Client(`postgres://${Configs.db.username}:${Configs.db.password}@localhost:5432/${Configs.db.name}`)
client.connect()

const jobs = kue.createQueue({
  prefix: 'q',
  redis: {
    port: 6379,
    host: 'localhost'
  }
})

module.exports = {
  jobs,
  queueEvents: () => {
    jobs.process('soon', (job, done) => {
      console.log(`job done: type - soon, ticket - ${job.data.ticket}`)
      const query = client.query(new Query(`SELECT user_id, type FROM tickets WHERE id = $1`, [job.data.ticket]))
      query.on('end', (ticket) => {
        if (ticket.rows[0].user_id && ticket.rows[0].type !== 'done') {
          require('../controllers/notification').addNotification(ticket.rows[0].user_id, job.data, 'soon')
        }
      })
      done(null, job.data)
    })

    jobs.process('late', (job, done) => {
      console.log(`job done: type - late, ticket - ${job.data.ticket}`)
      const query = client.query(new Query(`SELECT user_id, type FROM tickets WHERE id = $1`, [job.data.ticket]))
      query.on('end', (ticket) => {
        if (ticket.rows[0].user_id && ticket.rows[0].type !== 'done') {
          require('../controllers/notification').addNotification(ticket.rows[0].user_id, job.data, 'late')
        }
      })
      done(null, job.data)
    })

    jobs.process('nobody', (job, done) => {
      console.log(`job done: type - nobody, ticket - ${job.data.ticket}`)
      const query = client.query(new Query(`SELECT user_id, type FROM tickets WHERE id = $1`, [job.data.ticket]))
      query.on('end', (ticket) => {
        if (!ticket.rows[0].user_id && ticket.rows[0].type !== 'done') {
          client.query(new Query(`SELECT id FROM users WHERE role_id = (SELECT id FROM roles WHERE code = 'counselor' AND company_id = $1)`,
            [job.data.company]), (err, counselorId) => {
            if (err) done(err)
            else if (counselorId.rowCount > 0) {
              counselorId.rows.forEach(item => {
                require('../controllers/notification').addNotification(item.id, job.data, 'nobody')
              })
            }
          })
        }
      })
      done(null, job.data)
    })

    jobs.process('telegram', (job, done) => {
      console.log(`job done: type - telegram, user - ${job.data.user_id}`)
      client.query(new Query(`DELETE FROM telegram_accounts WHERE id = $1`, [job.data.id]))
      done(null, job.data)
    })
  }
}
