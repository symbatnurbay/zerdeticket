import nodemailer from 'nodemailer'
const utilPersonal = require('./personal')
const Configs = require('../config/config')

const sendMail = (to, subject = '', text = '', html = '', done) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: Configs.emailData.email,
      pass: Configs.emailData.password
    }
  })

  const mailOptions = {
    from: `ZERDETicket <${Configs.emailData.email}>`,
    to,
    subject,
    text,
    html
  }

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) console.log(err)
    console.log('Message sent:', info.messageId)
    done(null, 'Письмо отправлено')
  })
}

const sendReplyMail = (company, mail, done) => {
  const transporter = nodemailer.createTransport({
    host: company.smtp,
    port: 587,
    secure: false,
    auth: {
      user: company.email,
      pass: utilPersonal.decrypt(company.password)
    }
  })

  const mailOptions = {
    from: `${company.name} <${company.c_email}>`,
    to: mail.to,
    subject: mail.subject,
    text: mail.text,
    html: mail.html,
    inReplyTo: mail.inReplyTo,
    references: mail.references,
    attachments: mail.attachments
  }

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) return done(err)
    console.log('Message sent:', info.messageId)
    done(null, info.messageId)
  })
}

module.exports = {
  sendMail,
  sendReplyMail
}
