import crypto from 'crypto'
import jwt from 'jsonwebtoken'
const Configs = require('../config/config')

const encrypt = (text) => {
  var cipher = crypto.createCipher(Configs.crypto.algorithm, Configs.crypto.password)
  var crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex')
  return crypted
}

const decrypt = (text) => {
  var decipher = crypto.createDecipher(Configs.crypto.algorithm, Configs.crypto.password)
  var dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8')
  return dec
}

const setPassword = (password) => {
  var salt = crypto.randomBytes(16).toString('hex')
  var hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex')

  return { hash, salt }
}

const validPassword = (password, salt, hash) => {
  var newHash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex')
  return newHash === hash
}

const generateJwt = (user) => {
  const payload = {
    id: user.id,
    email: user.email,
    username: user.username,
    role: user.role_id,
    roleName: user.roleName,
    company: user.company_id
  }
  return jwt.sign(payload, Configs.auth.JWT_SECRET, { expiresIn: Configs.auth.tokenExpiry })
}

module.exports = {
  encrypt,
  decrypt,
  setPassword,
  validPassword,
  generateJwt
}
