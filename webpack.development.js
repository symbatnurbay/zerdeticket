const webpack = require('webpack')
var path = require('path')

// var ExtractTextPlugin = require('extract-text-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    client: [
      './client/index.js'
      // 'webpack-hot-middleware/client?path=http://localhost:8000/__webpack_hmr'
    ]
  },
  resolve: {
    extensions: ['.css', '.vue', '.js'],
    mainFiles: ['index'],
    alias: {
      vue: 'vue/dist/vue.js',
      Atoms: path.resolve(__dirname, './client/components/atoms/'),
      Organisms: path.resolve(__dirname, './client/components/organisms/'),
      Pages: path.resolve(__dirname, './client/components/pages/'),
      Molecules: path.resolve(__dirname, './client/components/molecules/'),
      EventBus: path.resolve(__dirname, './client/eventBus.js')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          cssModules: {
            localIdentName: '[path][name]---[local]---[hash:base64:5]',
            camelCase: true
          }
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)(\?.*)?$/,
        loader: 'url-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              camelCase: true,
              localIdentName: '[local]--[hash:base64:5]',
              importLoaders: 1
            }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './client/template.html'
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ],
  devtool: '#source-map'
}
